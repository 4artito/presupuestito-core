CREATE TABLE IF NOT EXISTS PERSONAS (
    id_persona  INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre      VARCHAR (100),
    apodo       VARCHAR (100),
    apellido    VARCHAR (100),
    dni_cuit    VARCHAR (20),
    saldo       FLOAT,
    direccion   VARCHAR (200),
    mail        VARCHAR (50),
    telefono    VARCHAR (50),
    descripcion TEXT,
    estado      TINYINT (1)   DEFAULT 1
                              NOT NULL
);

CREATE TABLE IF NOT EXISTS EMPLEADOS (
    id_empleado   INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_persona INTEGER (10) NOT NULL,
    sueldo        FLOAT (10),
    categoria     VARCHAR (50),
    periodo       DOUBLE (10),
    FOREIGN KEY (
        fk_id_persona
    )
    REFERENCES PERSONAS (id_persona) 
);

CREATE TABLE IF NOT EXISTS CLIENTES (
    id_cliente    INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_persona INTEGER (10) NOT NULL,
    FOREIGN KEY (
        fk_id_persona
    )
    REFERENCES PERSONAS (id_persona) 
);

CREATE TABLE IF NOT EXISTS PROVEEDORES (
    id_proveedor    INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_persona   INTEGER (10) NOT NULL,
    tel_contacto    VARCHAR (50),
    nombre_contacto VARCHAR (50),
    FOREIGN KEY (
        fk_id_persona
    )
    REFERENCES PERSONAS (id_persona) 
);

CREATE TABLE IF NOT EXISTS PRESUPUESTOS (
    id_presupuesto    INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_cliente     INTEGER (10) NOT NULL,
    fk_id_trabajo     INTEGER (10) NOT NULL,
    fecha_vencimiento DATE         NOT NULL,
    valor             DOUBLE       NOT NULL,
    estado            TEXT         NOT NULL,
    notas             TEXT,
    FOREIGN KEY (
        fk_id_cliente
    )
    REFERENCES CLIENTES (id_cliente),
    FOREIGN KEY (
        fk_id_trabajo
    )
    REFERENCES TRABAJOS (id_trabajo) 
);

CREATE TABLE IF NOT EXISTS TRABAJOS (
    id_trabajo     INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_material INTEGER (10) NOT NULL,
    fk_id_modelo   INTEGER (10) NOT NULL,
    horas          DOUBLE       NOT NULL,
    valor          DOUBLE       NOT NULL,
    fecha_entrega  DATE         NOT NULL,
    estado         TEXT         NOT NULL,
    adicional      DOUBLE       NOT NULL,
    ganancia       DOUBLE       NOT NULL,
    costo          DOUBLE       NOT NULL,
    notas          TEXT,
    es_tradicional BOOLEAN      DEFAULT 1,
    FOREIGN KEY (
        fk_id_material
    )
    REFERENCES MATERIALES (id_material),
    FOREIGN KEY (
        fk_id_modelo
    )
    REFERENCES MODELOS (id_modelo) 
);

CREATE TABLE IF NOT EXISTS COSTOS (
    id_costo              INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_tipos_de_costos INTEGER (10) NOT NULL,
    descripcion           TEXT         NOT NULL,
    valorPorHora          DOUBLE       NOT NULL,
    FOREIGN KEY (
        fk_id_tipos_de_costos
    )
    REFERENCES TIPOS_DE_COSTOS (id_tipo_de_costos) 
);

CREATE TABLE IF NOT EXISTS TIPOS_DE_COSTOS (
    id_tipo_de_costos INTEGER PRIMARY KEY AUTOINCREMENT,
    nombre            TEXT    NOT NULL,
    total             DOUBLE  NOT NULL,
    periodo           DOUBLE  NOT NULL,
    fecha_de_pago     DATE    NOT NULL
);

CREATE TABLE IF NOT EXISTS MODELOS (
    id_modelo   INTEGER        PRIMARY KEY AUTOINCREMENT,
    descripcion NVARCHAR (200) 
);

CREATE TABLE IF NOT EXISTS SUBRUBROS (
    id_subrubro INTEGER       PRIMARY KEY AUTOINCREMENT,
    fk_id_rubro INTEGER (10)  NOT NULL,
    nombre      VARCHAR (100) NOT NULL,
    FOREIGN KEY (
        fk_id_rubro
    )
    REFERENCES RUBRO (id_rubro) 
);

CREATE TABLE IF NOT EXISTS RUBRO (
    id_rubro INTEGER       PRIMARY KEY AUTOINCREMENT,
    nombre   VARCHAR (100) 
);

CREATE TABLE IF NOT EXISTS MATERIALES (
    id_material            INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_subrubro         INTEGER (10) NOT NULL,
    fk_id_unidad_de_medida INTEGER (10) NOT NULL,
    nombre                 VARCHAR (50) NOT NULL,
    descripcion            TEXT,
    FOREIGN KEY (
        fk_id_subrubro
    )
    REFERENCES SUBRUBROS (id_subrubro),
    FOREIGN KEY (
        fk_id_unidad_de_medida
    )
    REFERENCES UNIDADES_DE_MEDIDAS (id_unidad_de_medida) 
);

CREATE TABLE IF NOT EXISTS SUBMATERIALES (
    id_submaterial           INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_material_principal INTEGER (10) NOT NULL,
    fk_id_material_accesorio INTEGER (10) NOT NULL,
    cantidad                 INTEGER (10),
    FOREIGN KEY (
        fk_id_material_principal
    )
    REFERENCES MATERIALES (id_material),
    FOREIGN KEY (
        fk_id_material_accesorio
    )
    REFERENCES MATERIALES (id_material) 
);

CREATE TABLE IF NOT EXISTS UNIDADES_DE_MEDIDAS (
    id_unidad_de_medida INTEGER PRIMARY KEY AUTOINCREMENT,
    descripcion         TEXT    NOT NULL
);

CREATE TABLE IF NOT EXISTS PRECIOS_UNITARIOS (
    id_precio_unitario INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_material     INTEGER (10) NOT NULL,
    fk_id_proveedor    INTEGER (10) NOT NULL,
    precio             DOUBLE       NOT NULL,
    fecha              DATE         NOT NULL,
    FOREIGN KEY (
        fk_id_material
    )
    REFERENCES MATERIALES (id_material),
    FOREIGN KEY (
        fk_id_proveedor
    )
    REFERENCES PROVEEDORES (id_proveedor) 
);

CREATE TABLE IF NOT EXISTS REL_MODELO_SUBRUBRO (
    id_modelo   INTEGER      NOT NULL,
    id_subrubro INTEGER (10) NOT NULL,
    cantidad    INTEGER (10) NOT NULL,
    FOREIGN KEY (
        id_modelo
    )
    REFERENCES MODELOS (id_modelo),
    FOREIGN KEY (
        id_subrubro
    )
    REFERENCES SUBRUBROS (id_subrubro),
    PRIMARY KEY (
        id_modelo,
        id_subrubro
    )
);

CREATE TABLE IF NOT EXISTS REL_TRABAJO_MATERIAL (
    id_trabajo  INTEGER (10) NOT NULL,
    id_material INTEGER (10) NOT NULL,
    cantidad    INTEGER (10) NOT NULL,
    FOREIGN KEY (
        id_trabajo
    )
    REFERENCES TRABAJOS (id_trabajo),
    FOREIGN KEY (
        id_material
    )
    REFERENCES MATERIALES (id_material) 
);

CREATE TABLE IF NOT EXISTS TRANSACCIONES (
    id_transaccion INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_persona  INTEGER (10) NOT NULL,
    monto          FLOAT (10)   NOT NULL,
    fecha          DATETIME     NOT NULL,
    FOREIGN KEY (
        fk_id_persona
    )
    REFERENCES PERSONAS (id_persona) 
);

CREATE TABLE IF NOT EXISTS COMPRAS (
    numero_factura VARCHAR (100) PRIMARY KEY,
    fk_id_persona  INTEGER (10)  NOT NULL,
    total          FLOAT (10)    NOT NULL,
    fecha          DATE          NOT NULL,
    estado         TINYINT (1)   DEFAULT (1) 
                                 NOT NULL,
    FOREIGN KEY (
        fk_id_persona
    )
    REFERENCES PERSONAS (id_persona) 
);

CREATE TABLE IF NOT EXISTS PAGOS (
    id_pago               INTEGER      PRIMARY KEY AUTOINCREMENT,
    fk_id_tipos_de_costos INTEGER (10) NOT NULL,
    total                 DOUBLE       NOT NULL,
    fecha_de_pago         DATE         NOT NULL,
    FOREIGN KEY (
        fk_id_tipos_de_costos
    )
    REFERENCES TIPOS_DE_COSTOS (id_tipo_de_costos) 
);
