package presupuestito.core;

import ejecutarsqlscript.Script.ReadSQL;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.util.converter.LocalDateStringConverter;
import presupuestito.core.controller.costos.CtrlCostoSingleton;
import presupuestito.core.controller.costos.CtrlPagosSingleton;
import presupuestito.core.controller.costos.CtrlTipoDeCostoSingleton;
import presupuestito.core.model.costos.Costo;
import presupuestito.core.model.costos.Pago;
import presupuestito.core.model.costos.TipoDeCosto;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.dao.costos.PagosDAO;


public class PresupuestitoCore {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {    
        //ReadSQL de la lib EjecutarSQLite3Script.jar
        ReadSQL leerSql = new ReadSQL();
        //Directorio del .sql contenedor de las querys para crear las tablas
        File archivo = new File("DataBase/PresupuestitoV2.sql");
        
        //establecer ruta para la utilizacion de la base de datos
        Conexion.setUrlDataBase("DataBase/PresupuestitoV2.db");
        
        //se crean las tablas en el archivo creado en la linea anterior
        leerSql.importarQuery(archivo, Conexion.getUrlDataBase());
       
//        VentanaPrincipal vp = new VentanaPrincipal();
//        vp.setVisible(true);
//        vp.setLocationRelativeTo(null);
        
//        String fecha = LocalDate.of(2021, 3, 8).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
//        String fecha2 = LocalDate.of(2021, 3, 8).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
////        LocalDate fecha3 = LocalDate.now();
////        System.out.println(fecha3);
////        fecha3 = fecha3.minusYears(1);
////        System.out.println(fecha3);
        
//        CtrlEmpleadoSingleton ces = CtrlEmpleadoSingleton.getInstance();
        CtrlCostoSingleton ccs = CtrlCostoSingleton.getInstance();
////        CtrlTipoDeCostoSingleton ctcs = CtrlTipoDeCostoSingleton.getInstance();
////        
////        TipoDeCosto tipoDeCosto = new TipoDeCosto(8,"iva", 192);
//        ctcs.crearTipoCosto(tipoDeCosto);
        
        
//        ctcs.actualizarTipoDeCosto(tipoDeCosto);
        
//        List<TipoDeCosto> ltdc = ctcs.listarTipoCosto();
//        ltdc.forEach(System.out::println);
//        
//        float costoPorHoraSalarial = ctcs.costoSalarialPorHoraEmpleados("empleado");
//        
//        System.out.println(costoPorHoraSalarial);
    
////        Pago pago = new Pago(5,tipoDeCosto.getId_tipo_costo(), tipoDeCosto.getNombre(), tipoDeCosto.getPeriodo(), 78000, fecha);
        CtrlPagosSingleton cps = CtrlPagosSingleton.getInstance();
        List<Pago> listaPagosUltimoAño = cps.filtrarPagos(8, LocalDate.now());
        //cps.actualizarPago(pago);
//        cps.crearPagos(pago);
        //cps.eliminarPago(pago.getId_pago());
        List<Pago> listaPagos = cps.listarPagos();
        listaPagos.forEach(System.out::println);
        
        //imprimir lista de pagos del ultimo año
        System.out.println("");
        System.out.println("Lista de pagos del ultimo año:");
        listaPagosUltimoAño.forEach(System.out::println);
        
        float costoHorario = ccs.calcularCostoHorario(listaPagosUltimoAño);
        System.out.println(costoHorario);
        
        Costo nuevoCosto = new Costo("Mantenimiento de sierras", costoHorario, 8);
        ccs.crearCosto(nuevoCosto);
        
        List<Float> listaDeCostosHorarios = new ArrayList<>();
        listaDeCostosHorarios.add(costoHorario);
        listaDeCostosHorarios.add(456.0f);
        
        float sumaCostosHorario = ccs.sumarTodosCostosHorarios(listaDeCostosHorarios);
        System.out.println(sumaCostosHorario);
    }
}
