/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.personas.transacciones;

import java.util.Objects;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase representa el registro de compras de los proveedores 
 * @author Luis
 */
public class Compra {
    private String numero_factura;
    private Persona persona;
    private float total;
    private String fecha;
    
    public Compra(String numero_factura, int id_persona, float total, String fecha, float saldo){
        this.numero_factura = numero_factura;
        this.persona = new Persona();
        this.persona.setId_persona(id_persona);
        this.persona.setSaldo(saldo);
        this.total = total;
        this.fecha = fecha;
    }

    public String getNumero_factura() {
        return numero_factura;
    }

    public void setNumero_factura(String numero_factura) {
        this.numero_factura = numero_factura;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Compras{" + "numero_factura=" + numero_factura + ", persona= ID: " + persona.getId_persona() + " SALDO: "+persona.getSaldo()+ ", total=" + total + ", fecha=" + fecha + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.numero_factura);
        hash = 37 * hash + Objects.hashCode(this.persona);
        hash = 37 * hash + Float.floatToIntBits(this.total);
        hash = 37 * hash + Objects.hashCode(this.fecha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compra other = (Compra) obj;
        if (Float.floatToIntBits(this.total) != Float.floatToIntBits(other.total)) {
            return false;
        }
        if (!Objects.equals(this.numero_factura, other.numero_factura)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        return true;
    }
    
    
    
}
