/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.personas.transacciones;

import java.util.Objects;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los atributos y metodos de una Transaccion
 * @author Luis
 * @version 1.0
 */
public class Transaccion {
    private int id_transaccion;
    private Persona persona;
    private float monto;
    private String fecha;
    
    public Transaccion(
            int id_transaccion, float monto, String fecha,
            int id_persona
    ){
        this.id_transaccion = id_transaccion;
        this.monto = monto;
        this.fecha = fecha;
        //Datos Persona
        this.persona = new Persona();
        this.persona.setId_persona(id_persona);
    }
    
    public int getId_transaccion() {
        return id_transaccion;
    }

    public void setId_transaccion(int id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "id_transaccion=" + id_transaccion +""
                + ", persona=" + persona.getId_persona() + ", monto=" + monto + ", fecha=" + fecha + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id_transaccion;
        hash = 97 * hash + Objects.hashCode(this.persona);
        hash = 97 * hash + Float.floatToIntBits(this.monto);
        hash = 97 * hash + Objects.hashCode(this.fecha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaccion other = (Transaccion) obj;
        if (this.id_transaccion != other.id_transaccion) {
            return false;
        }
        if (Float.floatToIntBits(this.monto) != Float.floatToIntBits(other.monto)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        return true;
    }
    
    
}
