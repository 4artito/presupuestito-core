/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.personas;

import java.util.Objects;

/**
 * Esta clase contiene los atributos y metodos de una persona
 * @author Luis
 * @version 1.0
 * @param <T>
 * @see Persona
 */
public class Persona<T>{
    private int id_persona;
    private String nombre;
    private String apodo;
    private String apellido;
    private String dni_cuit;
    private float saldo;
    private String direccion;
    private String mail;
    private String telefono;
    private String descripcion;

    /**
     * Metodo constructor por defecto
     */
    public Persona(){
    }
    
    /**
     * Metodo constructor para listar: 
     *   las personas traidas de la base de datos
     * @param id_persona Identificador de la persona en la db
     * @param nombre Nombre de la persona
     * @param apodo Apodo de la persona
     * @param apellido Apellido de la persona
     * @param dni_cuit Dni o cuit de la persona
     * @param saldo Saldo de la persona
     * @param direccion Direccion de la persona
     * @param mail Mail de la persona
     * @param telefono Numero Telefonico de la persona
     * @param descripcion Alguna descripcion personal de la persona
     */
    public Persona(
            int id_persona, String nombre, String apodo, String apellido, String dni_cuit, 
            float saldo, String direccion, String mail, String telefono, String descripcion
    ) {
        this.id_persona = id_persona;
        this.nombre = nombre;
        this.apodo = apodo;
        this.apellido = apellido;
        this.dni_cuit = dni_cuit;
        this.saldo = saldo;
        this.direccion = direccion;
        this.mail = mail;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }

    /**
     * Metodo constructor para crear:
     *   una persona y mandarla a la base de datos
     * @param nombre Nombre de la persona
     * @param apodo Apodo de la persona
     * @param apellido Apellido de la persona
     * @param dni_cuit Dni o cuit de la persona
     * @param saldo Saldo de la persona
     * @param direccion Direccion de la persona
     * @param mail Mail de la persona
     * @param telefono Numero Telefonico de la persona
     * @param descripcion Alguna descripcion personal de la persona
     */
    public Persona(
            String nombre, String apodo, String apellido, String dni_cuit, float saldo,
            String direccion, String mail, String telefono, String descripcion
    ) {
        this.nombre = nombre;
        this.apodo = apodo;
        this.apellido = apellido;
        this.dni_cuit = dni_cuit;
        this.saldo = saldo;
        this.direccion = direccion;
        this.mail = mail;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }
    
    /**
     * Metodo que regresa el id de la persona
     * @return id_persona
     */
    public int getId_persona() {
        return id_persona;
    }

    /**
     * Metodo para establecer el id de la persona
     * @param id_persona
     */
    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    /**
     * Metodo que regresa el nombre de la persona
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo para establecer el nombre de la persona
     * @param nombre 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que regresa el apodo de la persona
     * @return apodo
     */
    public String getApodo() {
        return apodo;
    }

    /**
     * Metodo para establecer el apodo de la persona
     * @param apodo 
     */
    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    /**
     * Metodo que regresa el apellido de la persona
     * @return apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Metodo para establecer el apellido de la persona
     * @param apellido 
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Metodo que regresa la direccion de la persona
     * @return direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo para establecer la direccion de la persona
     * @param direccion 
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que regresa el mail de la persona
     * @return mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * Metodo para establecer el mail de la persona
     * @param mail 
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Metodo que regresa el telefono de la persona
     * @return telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo para establecer el telefono de la persona
     * @param telefono 
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que regresa una descripcion de la persona
     * @return descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo para establecer una descripcion a la persona
     * @param descripcion 
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Metodo que regresa el dni o cuit de la persona
     * @return dni_cuit
     */
    public String getDni_cuit() {
        return dni_cuit;
    }

    /**
     * Metodo para establecer el dni o cuit de la persona
     * @param dni_cuit 
     */
    public void setDni_cuit(String dni_cuit) {
        this.dni_cuit = dni_cuit;
    }

    /**
     * Metodo para regresar el saldo de la persona
     * @return saldo
     */
    public float getSaldo() {
        return saldo;
    }

    /**
     * Metodo para establecer el saldo de la persona
     * @param saldo 
     */
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    /**
     * Metodo que regresa una descripcion detallada de las personas con
     * todos los datos establecidos
     * @return 
     */
    @Override
    public String toString() {
        return "Persona{" + "id_persona=" + id_persona + ", nombre=" + nombre + ", apodo=" + apodo + ", apellido=" + apellido + ", dni/cuit=" + dni_cuit + ", saldo=" + saldo +", direccion=" + direccion + ", mail=" + mail + ", telefono=" + telefono + ", descripcion=" + descripcion + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.id_persona;
        hash = 23 * hash + Objects.hashCode(this.nombre);
        hash = 23 * hash + Objects.hashCode(this.apodo);
        hash = 23 * hash + Objects.hashCode(this.apellido);
        hash = 23 * hash + Objects.hashCode(this.dni_cuit);
        hash = 23 * hash + Float.floatToIntBits(this.saldo);
        hash = 23 * hash + Objects.hashCode(this.direccion);
        hash = 23 * hash + Objects.hashCode(this.mail);
        hash = 23 * hash + Objects.hashCode(this.telefono);
        hash = 23 * hash + Objects.hashCode(this.descripcion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona<?> other = (Persona<?>) obj;
        if (this.id_persona != other.id_persona) {
            return false;
        }
        if (Float.floatToIntBits(this.saldo) != Float.floatToIntBits(other.saldo)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apodo, other.apodo)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        if (!Objects.equals(this.dni_cuit, other.dni_cuit)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.telefono, other.telefono)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        return true;
    }
    
    
    
}
