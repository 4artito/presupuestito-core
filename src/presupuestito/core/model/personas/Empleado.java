package presupuestito.core.model.personas;

import java.util.Objects;

/**
 * Esta clase contiene los atributos y metodos de un empleado
 * @author Luis
 * @version 1.0
 * @see Persona
 */
public class Empleado extends Persona{
    private int id_empleado;
    private float sueldo;
    private String categoria;
    private float periodo;

    /**
     * Metodo constructor por defecto
     */
    public Empleado(){
    }
    
    /**
     * Metodo constructor para listar: 
     *   los empleados traidos de la base de datos
     * @param id_persona Identificador de la persona en la db
     * @param nombre Nombre del empleado
     * @param apodo Apodo del empleado
     * @param apellido Apellido del empleado
     * @param dni_cuit Dni o cuit del empleado
     * @param saldo Saldo del empleado
     * @param direccion Direccion donde vive el empleado
     * @param mail Mail del empleado
     * @param telefono Numero telefonico del empleado
     * @param descripcion Alguna descripcion personal del empleado
     * @param id_empleado Identificador del empleado en la db
     * @param sueldo Sueldo a cobrar por el empleado
     * @param categoria 
     * @param periodo Periodo de trabajo del empleado
     */
    public Empleado(
            int id_persona, String nombre, String apodo, String apellido, 
            String dni_cuit, float saldo, String direccion, String mail, 
            String telefono, String descripcion,
            int id_empleado, float sueldo, String categoria, float periodo
        ) {
        super(id_persona, nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
        this.id_empleado = id_empleado;
        this.sueldo = sueldo;
        this.categoria = categoria;
        this.periodo = periodo;
    }

    /**
     * Metodo constructor para crear:
     *   un empleado y mandarlo a la base de datos
     * @param nombre Nombre del empleado
     * @param apodo Apodo del empleado
     * @param apellido Apellido del empleado
     * @param direccion Direccion donde vive el empleado
     * @param mail Mail del empleado
     * @param telefono Telefono del empleado
     * @param descripcion Alguna descripcion personal del empleado
     * @param sueldo Sueldo a cobrar por el empleado
     * @param categoria 
     * @param periodo Periodo de trabajo del empleado
     */
    public Empleado(
            String nombre, String apodo, String apellido, String dni_cuit,
            float saldo, String direccion, 
            String mail, String telefono, String descripcion, float sueldo, 
            String categoria, float periodo
    ) {
        super(nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
        this.sueldo = sueldo;
        this.categoria = categoria;
        this.periodo = periodo;
    }
    
    /**
     * Metodo que regresa el id del empleado
     * @return id_empleado
     */
    public int getId_empleado() {
        return id_empleado;
    }
    
    /**
     * Metodo para establecer el id del empleado
     * @param id_empleado 
     */
    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    /**
     * Metodo que regresa el sueldo del empleado
     * @return sueldo 
     */
    public float getSueldo() {
        return sueldo;
    }
    
    /**
     * Metodo para establecer el sueldo del empleado
     * @param sueldo 
     */
    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    /**
     * Metodo que regresa la categoria del empleado
     * @return categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Metodo para establecer la categoria del empleado
     * @param categoria 
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Metodo que regresa el periodo del empleado
     * @return periodo
     */
    public float getPeriodo() {
        return periodo;
    }

    /**
     * Metodo para establecer el periodo del empleado
     * @param periodo 
     */
    public void setPeriodo(float periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.id_empleado;
        hash = 23 * hash + Float.floatToIntBits(this.sueldo);
        hash = 23 * hash + Objects.hashCode(this.categoria);
        hash = 23 * hash + Float.floatToIntBits(this.periodo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (this.id_empleado != other.id_empleado) {
            return false;
        }
        if (Float.floatToIntBits(this.sueldo) != Float.floatToIntBits(other.sueldo)) {
            return false;
        }
        if (Float.floatToIntBits(this.periodo) != Float.floatToIntBits(other.periodo)) {
            return false;
        }
        if (!Objects.equals(this.categoria, other.categoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona{ "+super.toString()+" } Empleado{" + "id_empleado=" + id_empleado + ", sueldo=" + sueldo + ", categoria=" + categoria + ", periodo=" + periodo + '}';
    }
    
    
    
}
