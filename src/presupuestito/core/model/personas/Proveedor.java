/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.personas;

import java.util.Objects;

/**
 * Esta clase contiene los atributos y metodos de un proveedor
 * @author Luis
 * @version 1.0
 * @see Persona
 */
public class Proveedor extends Persona{
    private int id_proveedor;
    private String tel_contacto;
    private String nombre_contacto;

    /**
     * Metodo constructor por defecto
     */
    public Proveedor(){
    }
    
    /**
     * Metodo constructor para listar: 
     *   los proveedores traidos de la base de datos
     * @param id_persona Identificador de la persona en la db
     * @param nombre Nombre del proveedor
     * @param apodo Apodo del proveedor
     * @param apellido Apellido del proveedor
     * @param dni_cuit Dni o cuit del proveedor
     * @param saldo Saldo del proveedor
     * @param direccion Direccion de el proveedor
     * @param mail Mail del proveedor 
     * @param telefono Numero Telefonico del proveedor
     * @param descripcion Alguna descripcion personal del proveedor
     * @param id_proveedor Identificador del proveedor en la db
     * @param tel_contacto Telefono de contacto del proveedor
     * @param nombre_contacto Nombre de contacto del proveedor
     */
    public Proveedor(
            int id_persona, String nombre, String apodo, String apellido,
            String dni_cuit, float saldo, String direccion,
            String mail, String telefono, String descripcion, int id_proveedor,
            String tel_contacto, String nombre_contacto
        ) {
        super(id_persona, nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
        this.id_proveedor = id_proveedor;
        this.tel_contacto = tel_contacto;
        this.nombre_contacto = nombre_contacto;
    }

    /**
     * Metodo constructor para crear:
     *   un proveedor y mandarlo a la base de datos
     * @param nombre Nombre del proveedor
     * @param apodo Apodo del proveedor
     * @param apellido Apellido del proveedor
     * @param dni_cuit Dni o cuit del proveedor
     * @param saldo Saldo del proveedor
     * @param direccion Direccion de el proveedor
     * @param mail Mail del proveedor 
     * @param telefono Numero Telefonico del proveedor
     * @param descripcion Alguna descripcion personal del proveedor
     * @param tel_contacto Telefono de contacto del proveedor
     * @param nombre_contacto Nombre de contacto del proveedor
     */
    public Proveedor(
            String nombre, String apodo, String apellido, String dni_cuit, float saldo,
            String direccion, String mail, String telefono, String descripcion, 
            String tel_contacto, String nombre_contacto
        ) {
        super(nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
        this.tel_contacto = tel_contacto;
        this.nombre_contacto = nombre_contacto;
    }

    /**
     * Metodo que regresa el id del proveedor
     * @return id_proveedor
     */
    public int getId_proveedor() {
        return id_proveedor;
    }

    /**
     * Metodo para establecer el id del proveedor
     * @param id_proveedor 
     */
    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    /**
     * Metodo que regresa el telefono de contacto del proveedor
     * @return tel_contacto
     */
    public String getTel_contacto() {
        return tel_contacto;
    }

    /**
     * Metodo para establecer el telefono de contacto del proveedor
     * @param tel_contacto 
     */
    public void setTel_contacto(String tel_contacto) {
        this.tel_contacto = tel_contacto;
    }

    /**
     * Metodo que regresa el nombre de contacto del proveedor
     * @return nombre_contacto
     */
    public String getNombre_contacto() {
        return nombre_contacto;
    }

    /**
     * Metodo para establecer el nombre de contacto del proveedor
     * @param nombre_contacto 
     */
    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id_proveedor;
        hash = 23 * hash + Objects.hashCode(this.tel_contacto);
        hash = 23 * hash + Objects.hashCode(this.nombre_contacto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proveedor other = (Proveedor) obj;
        if (this.id_proveedor != other.id_proveedor) {
            return false;
        }
        if (!Objects.equals(this.tel_contacto, other.tel_contacto)) {
            return false;
        }
        if (!Objects.equals(this.nombre_contacto, other.nombre_contacto)) {
            return false;
        }
        return true;
    }
    
    
    
}
