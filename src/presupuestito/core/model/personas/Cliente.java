package presupuestito.core.model.personas;

/**
 * Esta clase contiene los atributos y metodos de un cliente
 * @author Luis
 * @version 1.0
 * @see Persona
 */
public class Cliente extends Persona{
    private int id_cliente;

    /**
     * Metodo constructor por defecto
     */
    public Cliente(){
    }
    
    /**
     * Metodo constructor para listar: 
     *   los clientes traidos de la base de datos
     * @param id_persona Identificador de la persona en la db
     * @param nombre Nombre del cliente
     * @param apodo Apodo del cliente
     * @param apellido Apellido del cliente
     * @param dni_cuit Dni o cuit del cliente
     * @param saldo Saldo del cliente
     * @param direccion Direccion donde vive el cliente
     * @param mail Mail del cliente 
     * @param telefono Numero Telefonico del cliente
     * @param descripcion Alguna descripcion personal del cliente
     * @param id_cliente Identificador del cliente en la db
     */
    public Cliente(
            int id_persona, String nombre, String apodo, String apellido,
            String dni_cuit, float saldo, String direccion,
            String mail, String telefono, String descripcion, int id_cliente
        ) {
        super(id_persona, nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
        this.id_cliente = id_cliente;
    }

    /**
     * Metodo constructor para crear:
     *   un cliente y mandarlo a la base de datos
     * @param nombre Nombre del cliente
     * @param apodo Apodo del cliente
     * @param apellido Apellido del cliente
     * @param dni_cuit Dni o cuit del cliente
     * @param saldo Saldo del cliente
     * @param direccion Direccion donde vive el cliente
     * @param mail Mail del cliente 
     * @param telefono Numero Telefonico del cliente
     * @param descripcion Alguna descripcion personal del cliente
     */
    public Cliente(
            String nombre, String apodo, String apellido, String dni_cuit,
            float saldo, String direccion,
            String mail, String telefono, String descripcion
        ) {
        super(nombre, apodo, apellido, dni_cuit, saldo, direccion, mail, telefono, descripcion);
    }

    /**
     * Metodo que regresa el id del cliente
     * @return id_cliente
     */
    public int getId_cliente() {
        return id_cliente;
    }

    /**
     * Metodo para establecer el id del cliente
     * @param id_cliente 
     */
    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }  

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + this.id_cliente;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (this.id_cliente != other.id_cliente) {
            return false;
        }
        return true;
    }
    
    
 
}
