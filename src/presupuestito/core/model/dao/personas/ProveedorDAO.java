package presupuestito.core.model.dao.personas;

import presupuestito.core.model.dao.personas.PersonaDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.personas.Persona;
import presupuestito.core.model.personas.Proveedor;
import presupuestito.core.model.dao.interfaces.PersonasSQL;

/**
 * Esta clase contiene los metodos y atributos para realizar consultas a la base de datos
 * en la tabla PROVEEDOR
 * @author Luis
 * @version 1.0
 */
public class ProveedorDAO implements PersonasSQL{
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo para crear proveedores en la base de datos
     * Regresa el numero de filas afectadas en la base de datos
     * @param proveedor Proveedor que se creara en la base de datos
     * @return respuesta
     */
    @Override
    public int create(Persona proveedor) {
        Connection cc = Conexion.getConexion();
        
        int respuesta = 0;
        if(proveedor instanceof Proveedor){
            Proveedor pr = (Proveedor)proveedor;
            try {

                this.sql = "SELECT count(personas.direccion) FROM personas "
                        + "INNER JOIN "
                        + "proveedores ON personas.id_persona = proveedores.fk_id_persona "
                        + "WHERE personas.direccion='"+pr.getDireccion()+"'";

                this.st = cc.createStatement();
                this.datos = this.st.executeQuery(this.sql);

                while(this.datos.next()){
                    if(this.datos.getString("count(personas.direccion)").equals("0"))
                    {
                        Persona p = new Persona(
                                pr.getNombre(), pr.getApellido(), pr.getApellido(),
                                pr.getDni_cuit(), pr.getSaldo(),
                                pr.getDireccion(), pr.getMail(), pr.getTelefono(),
                                pr.getDescripcion()
                        );
                        PersonaDAO pdao = new PersonaDAO();
                        pdao.create(p);

                        this.sql = "INSERT INTO proveedores(fk_id_persona, tel_contacto, nombre_contacto) "
                                + "VALUES (?,?,?)";

                        this.pst = cc.prepareStatement(this.sql);
                        this.pst.setInt(1, p.getId_persona());
                        this.pst.setString(2, pr.getTel_contacto());
                        this.pst.setString(3, pr.getNombre_contacto());

                        respuesta = this.pst.executeUpdate();

                        this.sql = "SELECT last_insert_rowid() as id_proveedor";
                        this.datos = this.st.executeQuery(this.sql);

                        if(this.datos.next())
                            pr.setId_persona(this.datos.getInt("id_proveedor"));

                        while(this.datos.next())
                            pr = new Proveedor(
                                    this.datos.getInt("id_persona"), this.datos.getString("nombre"), 
                                    this.datos.getString("apodo"), this.datos.getString("apellido"),
                                    this.datos.getString("dni_cuit"), this.datos.getFloat("saldo"),
                                    this.datos.getString("direccion"), this.datos.getString("mail"),
                                    this.datos.getString("telefono"), this.datos.getString("descripcion"),
                                    this.datos.getInt("id_proveedor"),
                                    this.datos.getString("tel_contacto"), this.datos.getString("nombre_contacto")
                            );
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Conexion.closeConnection();
            }
        }
        return respuesta;
    }
    
    /**
     * Metodo que trae la lista de proveedores de la base de datos
     * Regresa todas las filas en una lista
     * @param estado indicara el estado actual de la lista que se 
     * traera de la base de datos (1 = activos | 0 = desactivos)
     * @param limit indicara el limite de la lista que trae
     * @param offset indicara a partir de que indice trae los datos
     * @return listaProveedores
     */
    @Override
    public List<Proveedor> listar(int estado, int limit, int offset){
        List<Proveedor> listaProveedores =  new ArrayList<>();
        Proveedor proveedor;
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT * FROM personas "
                + "INNER JOIN proveedores "
                + "ON personas.id_persona = proveedores.fk_id_persona "
                + "WHERE personas.estado = "+estado+" "
                + "ORDER BY personas.id_persona "
                + "LIMIT "+limit+" OFFSET "+offset+"";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while(this.datos.next()){
                proveedor = new Proveedor(
                        this.datos.getInt(1), this.datos.getString(2), this.datos.getString(3),
                        this.datos.getString(4), this.datos.getString(5), this.datos.getFloat(6),
                        this.datos.getString(7), this.datos.getString(8), this.datos.getString(9),
                        this.datos.getString(10), 
                        this.datos.getInt("id_proveedor"), this.datos.getString("tel_contacto"), 
                        this.datos.getString("nombre_contacto")
                );
                listaProveedores.add(proveedor);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaProveedores;
    }
    
    /**
     * Metodo para actualizar algun dato del proveedor en la base de datos
     * Regresa el numero de filas afectadas en la base de datos
     * @param proveedor Proveedor que va a ser actualizado en la base de datos
     * @return respuesta
     */
    @Override
    public int update(Persona proveedor){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        if(proveedor instanceof Proveedor){
            Proveedor pr = (Proveedor)proveedor;
            try {
                Persona p = new Persona(
                                pr.getId_persona(), pr.getNombre(), pr.getApodo(),
                                pr.getApellido(), pr.getDni_cuit(), pr.getSaldo(),
                                pr.getDireccion(), pr.getMail(),
                                pr.getTelefono(), pr.getDescripcion()
                );
                PersonaDAO pdao = new PersonaDAO();
                pdao.update(p);

                this.sql = "UPDATE proveedores SET tel_contacto=?, nombre_contacto=? WHERE id_proveedor=?";

                this.pst = cc.prepareStatement(this.sql);

                this.pst.setString(1, pr.getTel_contacto());
                this.pst.setString(2, pr.getNombre_contacto());
                this.pst.setInt(3, pr.getId_proveedor());

                respuesta = this.pst.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Conexion.closeConnection();
            }
        }
        return respuesta;
    }
}
