/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.personas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.personas.Empleado;
import presupuestito.core.model.personas.Persona;
import presupuestito.core.model.dao.interfaces.PersonasSQL;

/**
 * Esta clase contiene los metodos y atributos para realizar consultas a la base de datos
 * en la tabla EMPLEADOS
 * @author Luis
 * @version 1.0
 */
public class EmpleadoDAO implements PersonasSQL{
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo para crear empleados en la base de datos
     * Regresa el numero de filas afectas en la base de datos
     * @param e Empleado que se creara en la base de datos
     * @return respuesta
     */
    @Override
    public int create(Persona e) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        if(e instanceof Empleado){
            Empleado empleado = (Empleado)e;

            try {

                this.sql = "SELECT count(personas.direccion) FROM personas "
                        + "INNER JOIN "
                        + "empleados ON personas.id_persona = empleados.fk_id_persona "
                        + "WHERE personas.direccion='"+empleado.getDireccion()+"'";

                this.st = cc.createStatement();
                this.datos = this.st.executeQuery(this.sql);

                while(this.datos.next()){
                    if(this.datos.getString("count(personas.direccion)").equals("0"))
                    {
                        Persona p = new Persona(
                                empleado.getNombre(), empleado.getApellido(), empleado.getApellido(),
                                empleado.getDni_cuit(), empleado.getSaldo(),
                                empleado.getDireccion(), empleado.getMail(), empleado.getTelefono(),
                                empleado.getDescripcion()
                        );
                        PersonaDAO pdao = new PersonaDAO();
                        pdao.create(p);

                        this.sql = "INSERT INTO empleados(fk_id_persona, sueldo, categoria, periodo) "
                                + "VALUES (?,?,?,?)";


                        this.pst = cc.prepareStatement(this.sql);
                        this.pst.setInt(1, p.getId_persona());
                        this.pst.setFloat(2, empleado.getSueldo());
                        this.pst.setString(3, empleado.getCategoria());
                        this.pst.setFloat(4, empleado.getPeriodo());

                        respuesta = this.pst.executeUpdate();

                        this.sql = "SELECT last_insert_rowid() as id_empleado";
                        this.datos = this.st.executeQuery(this.sql);

                        if(this.datos.next())
                            e.setId_persona(this.datos.getInt("id_empleado"));

                        while(this.datos.next())
                            e = new Empleado(
                                    this.datos.getInt("id_persona"), this.datos.getString("nombre"), 
                                    this.datos.getString("apodo"), this.datos.getString("apellido"),
                                    this.datos.getString("dni_cuit"), this.datos.getFloat("saldo"),
                                    this.datos.getString("direccion"), this.datos.getString("mail"),
                                    this.datos.getString("telefono"), this.datos.getString("descripcion"),
                                    this.datos.getInt("id_empleado"), this.datos.getFloat("sueldo"), 
                                    this.datos.getString("categoria"), this.datos.getFloat("periodo"));
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Conexion.closeConnection();
            }

        }
        return respuesta;
    }
    
    /**
     * Metodo que trae la lista de empleados de la base de datos
     * Regresa todas las filas en una lista
     * @param estado indicara el estado actual de la lista que se
     * traera de la base de datos (1 = activos | 0 = desactivos)
     * @param limit indicara el limite de la lista que trae
     * @param offset indicara a partir de que indice trae los datos
     * @return listaEmpleados
     */
    @Override
    public List<Empleado> listar(int estado, int limit, int offset){
        List<Empleado> listaEmpleados =  new ArrayList<>();
        Empleado empleado;
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT * FROM personas "
                    + "INNER JOIN empleados "
                    + "ON personas.id_persona = empleados.fk_id_persona "
                    + "WHERE personas.estado = "+estado+" "
                    + "ORDER BY personas.id_persona "
                    + "LIMIT "+limit+" OFFSET "+offset+"";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while(this.datos.next()){
                empleado = new Empleado(
                        this.datos.getInt(1), this.datos.getString(2), 
                        this.datos.getString(3), this.datos.getString(4), this.datos.getString(5), 
                        this.datos.getFloat(6), this.datos.getString(7), this.datos.getString(8),
                        this.datos.getString(9), this.datos.getString(10),
                        this.datos.getInt("id_empleado"), this.datos.getFloat("sueldo"),
                        this.datos.getString("categoria"), this.datos.getFloat("periodo"));
                listaEmpleados.add(empleado);
            }
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        
        return listaEmpleados;
    }
    
    /**
     * Metodo para actualizar un empleado en la base de datos
     * Regresa el numero de filas afectas en la base de datos
     * @param e Empleado que va a ser actualizado en la base de datos
     * @return respuesta
     */
    @Override
    public int update(Persona e){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        try {
            Persona p = new Persona(
                            e.getId_persona(), e.getNombre(), e.getApodo(),
                            e.getApellido(), e.getDni_cuit(), e.getSaldo(),
                            e.getDireccion(), e.getMail(),
                            e.getTelefono(), e.getDescripcion()
            );
            PersonaDAO pdao = new PersonaDAO();
            pdao.update(p);
            
            this.sql = "UPDATE empleados SET sueldo=?, categoria=?, periodo=? WHERE id_empleado=?";
            
            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setFloat(1, ((Empleado)e).getSueldo());
            this.pst.setString(2, ((Empleado)e).getCategoria());
            this.pst.setFloat(3, ((Empleado)e).getPeriodo());
            this.pst.setInt(4, ((Empleado)e).getId_empleado());
            
            respuesta = this.pst.executeUpdate();
    
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    /**
     * Metodo para sumar los sueldos de todos los empleados
     * @return 
     */
    public float sumarSueldos(){
        Connection cc = Conexion.getConexion();
        float respuesta = 0;
        
        try {
            this.sql = "SELECT sum(sueldo) FROM EMPLEADOS "
                    + "INNER JOIN PERSONAS ON EMPLEADOS.fk_id_persona = PERSONAS.id_persona "
                    + "WHERE PERSONAS.estado = 1";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            if (this.datos.next()){                
                respuesta = this.datos.getFloat("sum(sueldo)");
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return respuesta;
    }
    
}
