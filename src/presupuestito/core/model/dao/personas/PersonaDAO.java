package presupuestito.core.model.dao.personas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los metodos y atributos para realizar consultas a la base de datos
 * en la tabla PERSONAS
 * @author Luis
 * @version 1.0
 */
public class PersonaDAO {
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo para crear una persona en la base de datos
     * @param p Persona que se va a crear en la base de datos
     */
    public void create(Persona p) {
        Connection cc = Conexion.getConexion();
        try {
            this.sql = "INSERT INTO personas(nombre,apodo,apellido,dni_cuit,saldo,direccion,mail,"
            + "telefono,descripcion) VALUES (?,?,?,?,?,?,?,?,?)";

            this.pst = cc.prepareStatement(this.sql);
            this.pst.setString(1, p.getNombre());
            this.pst.setString(2, p.getApodo());
            this.pst.setString(3, p.getApellido());
            this.pst.setString(4, p.getDni_cuit());
            this.pst.setFloat(5, p.getSaldo());
            this.pst.setString(6, p.getDireccion());
            this.pst.setString(7, p.getMail());
            this.pst.setString(8, p.getTelefono());
            this.pst.setString(9, p.getDescripcion());

            this.pst.executeUpdate();

            this.sql = "SELECT last_insert_rowid() as id_persona";
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);

            if(this.datos.next())
                p.setId_persona(this.datos.getInt("id_persona"));

            while(this.datos.next())
                p = new Persona(this.datos.getString(1), this.datos.getString(2),
                                this.datos.getString(3), this.datos.getString(4),
                                this.datos.getFloat(5), this.datos.getString(6),
                                this.datos.getString(7), this.datos.getString(8),
                                this.datos.getString(9));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }  
    
    /**
     * Metodo para actualizar una persona en la base de datos
     * @param p Persona que va a ser actualizada
     */
    public void update(Persona p){
        Connection cc = Conexion.getConexion();
        try {
            
            this.sql = "UPDATE personas SET nombre=?, apodo=?, apellido=?,"
                    + "dni_cuit=?, saldo=?, "
                    + "direccion=?, mail=?, telefono=?, descripcion=? "
                    + "WHERE id_persona =?";

            this.pst = cc.prepareStatement(this.sql);
            this.pst.setString(1, p.getNombre());
            this.pst.setString(2, p.getApodo());
            this.pst.setString(3, p.getApellido());
            this.pst.setString(4, p.getDni_cuit());
            this.pst.setFloat(5, p.getSaldo());
            this.pst.setString(6, p.getDireccion());
            this.pst.setString(7, p.getMail());
            this.pst.setString(8, p.getTelefono());
            this.pst.setString(9, p.getDescripcion());
            this.pst.setInt(10, p.getId_persona());
            
            this.pst.executeUpdate();
          
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Metodo para realizar un borrado logico de una persona
     * Regresa la cantidad de filas afectadas en la base de datos
     * @param id Identificador de la persona
     * @return respuesta
     */
    public int borradoLogico(int id) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        try {
            
            this.sql = "UPDATE personas SET estado=0 WHERE id_persona =?";
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setInt(1, id);
            
            respuesta = this.pst.executeUpdate();
         
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    
    /**
     * Metodo para realizar una alta logica a una persona
     * Regresa la cantidad de filas afectadas en la base de datos
     * @param id Identificador de la persona
     * @return respuesta
     */
    public int altaLogica(int id) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        try {
            
            this.sql = "UPDATE personas SET estado=1 WHERE id_persona =?";
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setInt(1, id);
            
            respuesta = this.pst.executeUpdate();
         
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
}
