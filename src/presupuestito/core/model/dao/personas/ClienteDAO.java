/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.personas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.personas.Cliente;
import presupuestito.core.model.personas.Persona;
import presupuestito.core.model.dao.interfaces.PersonasSQL;

/**
 * Esta clase contiene los metodos y atributos para realizar consultas a la base de datos
 * en la tabla CLIENTES
 * @author Luis
 * @version 1.0
 */
public class ClienteDAO implements PersonasSQL{
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo para crear clientes en la base de datos
     * Regresa el numero de filas afectadas en la base de datos
     * @param cliente Cliente que se creara en la base de datos
     * @return respuesta
     */
    @Override
    public int create(Persona cliente) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        if(cliente instanceof Cliente){
            Cliente c = (Cliente)cliente;
            try {
                this.sql = "SELECT count(personas.direccion) FROM personas "
                        + "INNER JOIN "
                        + "clientes ON personas.id_persona = clientes.fk_id_persona "
                        + "WHERE personas.direccion='"+c.getDireccion()+"'";

                this.st = cc.createStatement();
                this.datos = this.st.executeQuery(sql);

                while(datos.next()){
                    if(this.datos.getString("count(personas.direccion)").equals("0"))
                    {
                        Persona p = new Persona(
                                c.getNombre(), c.getApellido(), c.getApellido(),
                                c.getDni_cuit(), c.getSaldo(),
                                c.getDireccion(), c.getMail(), c.getTelefono(),
                                c.getDescripcion()
                        );
                        PersonaDAO pdao = new PersonaDAO();
                        pdao.create(p);

                        this.sql = "INSERT INTO clientes(fk_id_persona) "
                                + "VALUES (?)";

                        this.pst = cc.prepareStatement(this.sql);
                        this.pst.setInt(1, p.getId_persona());

                        respuesta = this.pst.executeUpdate();

                        this.sql = "SELECT last_insert_rowid() as id_cliente";
                        this.datos = this.st.executeQuery(this.sql);

                        if(this.datos.next())
                            c.setId_persona(this.datos.getInt("id_cliente"));

                        while(this.datos.next())
                            c = new Cliente(
                                    this.datos.getInt("id_persona"), datos.getString("nombre"), 
                                    this.datos.getString("apodo"), datos.getString("apellido"),
                                    this.datos.getString("dni_cuit"), this.datos.getFloat("saldo"),
                                    this.datos.getString("direccion"), datos.getString("mail"),
                                    this.datos.getString("telefono"), datos.getString("descripcion"),
                                    this.datos.getInt("id_empleado")
                            );
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Conexion.closeConnection();
            }
        }
        
        return respuesta;
    }
    
    /**
     * Metodo que trae la lista de clientes de la base de datos
     * Regresa todas las filas en una lista
     * @param estado indicara el estado actual de la lista que se 
     * traera de la base de datos (1 = activos | 0 = desactivos)
     * @param limit indicara el limite de la lista que trae
     * @param offset indicara a partir de que indice trae los datos
     * @return listaClientes
     */
    @Override
    public List<Cliente> listar(int estado, int limit, int offset){
        List<Cliente> listaClientes =  new ArrayList<>();
        Cliente cliente;
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT * FROM personas "
                + "INNER JOIN clientes "
                + "ON personas.id_persona = clientes.fk_id_persona "
                + "WHERE personas.estado = "+estado+" "
                + "ORDER BY personas.id_persona "
                + "LIMIT "+limit+" OFFSET "+offset+"";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while(this.datos.next()){
                cliente = new Cliente(
                        this.datos.getInt(1), this.datos.getString(2), 
                        this.datos.getString(3), this.datos.getString(4), this.datos.getString(5), 
                        this.datos.getFloat(6), this.datos.getString(7), this.datos.getString(8),
                        this.datos.getString(9), this.datos.getString(10),
                        this.datos.getInt("id_cliente")
                );
                listaClientes.add(cliente);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaClientes;
    }
    
    /**
     * Metodo para actualizar algun dato del cliente en la base de datos
     * Regresa el numero de filas afectadas en la base de datos
     * @param cliente Cliente que va a ser actualizado en la base de datos
     * @return respuesta
     */
    @Override
    public int update(Persona cliente){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        if(cliente instanceof Cliente){
            Cliente c = (Cliente)cliente;
            try {
                Persona p = new Persona(
                                c.getId_persona(), c.getNombre(), c.getApodo(),
                                c.getApellido(), c.getDni_cuit(), c.getSaldo(),
                                c.getDireccion(), c.getMail(),
                                c.getTelefono(), c.getDescripcion()
                );
                PersonaDAO pdao = new PersonaDAO();
                pdao.update(p);

                //A revisar si es posible, le puse que actualize el fk_id_persona para obtener una respuesta
                this.sql = "UPDATE clientes SET fk_id_persona=? WHERE id_cliente = ?";

                this.pst = cc.prepareStatement(this.sql);
                this.pst.setInt(1, c.getId_persona());
                this.pst.setInt(2, c.getId_cliente());

                respuesta = this.pst.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Conexion.closeConnection();
            }
        }
        return respuesta;
    }
    
    
}
