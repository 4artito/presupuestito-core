package presupuestito.core.model.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Esta clase contiene los atributos y metodos para establecer una coneccion a la base de datos
 * @author Luis
 * @version 1.0
 */
public class Conexion {
    private static Connection connection = null;
    private static String url;
    
    /**
     * Metodo constructor privado por defecto
     */
    private Conexion(){
        initConnection();
    }
    
    /**
     * Metodo que pregunta si la connection es null y regresa la coneccion a la base de datos
     * @return connection
     */
    public static Connection getConexion(){
        if (connection == null){
            new Conexion();
        }
        return connection;
    }
    
    /**
     * Metodo privado para iniciar la conexion a la base de datos
     */
    private void initConnection(){
        
        try
        {
            connection = DriverManager.getConnection("jdbc:sqlite:"+this.url);
            if(connection != null){
                System.out.println("Conectado");
            }
        }
        catch(SQLException e)
        {
            System.out.println("La conexion ya esta establecida");
        }
        catch (Exception e) {
            System.out.println("El directorio no existe");
        }
    }
    
    /**
     * Metodo publico que cierra la conexion a la base de datos
     * Y adicionalmente se requiere volver NULL el valor de la variable Connection
     */
    public static void closeConnection(){
        try {
            connection.close();
            connection = null;
            if(connection == null){
                System.out.println("Desconectado");
            }
        } catch (SQLException | NullPointerException ex ) {
            System.out.println("La conexion ya se cerro");
        }
    }
    
    /**
     * Metodo para establecer el directorio donde se encuentra o se desea crear la base de datos
     * Tambien crea el directorio si este no existe
     * Recomiendo que este metodo sea la primer linea de codigo a ejecutar en el Main
     * @param directorio String con el directorio de la base de datos
     */
    public static void setUrlDataBase(String directorio){
        crearDirectorio(directorio);
        url = directorio;
    }
    
    /**
     * Metodo que retorna el directorio de la base de datos
     * @return url
     */
    public static String getUrlDataBase() {
        return url;
    }
    
    private static void crearDirectorio(String directorio){
        String mkDir = splitDirectorio(directorio);
        
        File ndirectorio = (File) new File(mkDir);
        if(!ndirectorio.exists()){
            if(ndirectorio.mkdirs()){
                System.out.println("directorio creado");
            } else {
                System.out.println("error al crear directorio");
            }
        } else {
            System.out.println("El directorio ya existe");
        }
    }
    
    private static String splitDirectorio(String directorio){
        
        String[] ndir = directorio.split("[/\\\\]");
        
        String mkDir = "";
        try {
            for (int i = 0; i < ndir.length-1; i++){
                if(i==0){
                    mkDir += ndir[i];
                } else {
                    mkDir += "/"+ndir[i];
                }
            }
        } catch (NullPointerException npe) {
            System.out.println(npe.getMessage());
        }
        
        return mkDir;
    }
}
