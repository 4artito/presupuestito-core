package presupuestito.core.model.dao.interfaces;

import java.util.List;

import presupuestito.core.model.personas.Persona;

public interface PersonasSQL {
    
    public List<? extends Persona> listar(int estado, int limit, int offset);
    
    public int create(Persona persona);
    
    public int update(Persona persona);
    
}
