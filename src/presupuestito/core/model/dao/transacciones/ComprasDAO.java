/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.transacciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import presupuestito.core.model.dao.Conexion;
import presupuestito.core.model.personas.transacciones.Compra;
import presupuestito.core.model.personas.Cliente;

/**
 *
 * @author Luis
 */
public class ComprasDAO {
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    
    public int create(Compra compra){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "SELECT count(numero_factura) FROM compras "
                    + "WHERE numero_factura = '"+compra.getNumero_factura()+"'";
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            if(this.datos.next()){
                if(this.datos.getString("count(numero_factura)").equals("0")){
                    this.sql = "INSERT INTO compras(numero_factura, fk_id_persona, total, fecha) "
                            + "VALUES (?, ?, ?, ?)";

                    this.pst = cc.prepareStatement(this.sql);
                    this.pst.setString(1, compra.getNumero_factura());
                    this.pst.setInt(2, compra.getPersona().getId_persona());
                    this.pst.setFloat(3, compra.getTotal()); 
                    this.pst.setString(4, compra.getFecha());

                    respuesta = this.pst.executeUpdate();

                } else {
                    respuesta = modificarCompra(compra);
                }
            } 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    public List<Compra> listar(int id, int estado){
        List<Compra> listaCompras = new ArrayList<>();
        Compra registro;
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT c.*, p.id_persona, p.saldo FROM COMPRAS c "
                    + "INNER JOIN PERSONAS p ON c.fk_id_persona = p.id_persona "
                    + "WHERE p.id_persona = '"+id+"' AND c.estado = '"+estado+"'";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while(this.datos.next()){
                registro = new Compra(
                        this.datos.getString("numero_factura"), this.datos.getInt("fk_id_persona"),
                        this.datos.getFloat("total"), this.datos.getString("fecha"), this.datos.getFloat("saldo")
                );
                listaCompras.add(registro);
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaCompras;
    }
    
    public int modificarCompra(Compra compra){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "UPDATE compras SET numero_factura = ?, "
                        + "fk_id_persona = ?, "
                        + "total = ?, "
                        + "fecha = ? "
                    + "WHERE numero_factura = ? ";
            
            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setString(1, compra.getNumero_factura());
            this.pst.setInt(2, compra.getPersona().getId_persona());
            this.pst.setFloat(3, compra.getTotal());
            this.pst.setString(4, compra.getFecha());
            this.pst.setString(5, compra.getNumero_factura());
            
            respuesta = this.pst.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    public int borradoLogicoCompra(String numeroFactura){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "UPDATE compras SET estado=0 WHERE numero_factura=?";
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setString(1, numeroFactura);
            
            respuesta = this.pst.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
}
