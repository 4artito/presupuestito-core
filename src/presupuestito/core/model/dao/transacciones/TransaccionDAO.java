/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.transacciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.dao.Conexion;

import presupuestito.core.model.personas.transacciones.Transaccion;

/**
 *
 * @author Luis
 */
public class TransaccionDAO {
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    //ESTOS 2 ULTIMOS METODOS PASAR A UN DAO DE TRANSACCIONES!
    public int transaccionEmpleado(int id, float monto, String fecha){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "INSERT INTO transacciones (fk_id_persona,monto,fecha) "
                    + "VALUES (?,?,?)";
            
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setInt(1, id);
            this.pst.setFloat(2, monto);
            this.pst.setString(3, fecha);
            
            respuesta = this.pst.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    public List<Transaccion> listarTransaccionPersonas(int id){
        Connection cc = Conexion.getConexion();
        List<Transaccion> listaTransaccion = new ArrayList<>();
        Transaccion transaccion = null;
        try {
            
            this.sql = "SELECT t.* ,p.id_persona FROM TRANSACCIONES t "
                + "INNER JOIN PERSONAS p ON t.fk_id_persona = p.id_persona "
                + "WHERE p.id_persona = '"+id+"';";
            
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while (this.datos.next()){
                transaccion = new Transaccion(
                        this.datos.getInt("id_transaccion"),
                        this.datos.getFloat("monto"), this.datos.getString("fecha"),
                        this.datos.getInt("id_persona")
                );
                
                listaTransaccion.add(transaccion);
            }
           
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaTransaccion;
    }
    
    //REUTILIZAMOS ESTE METODO?
    public int updateSaldo(int id, float total){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "UPDATE personas SET saldo=saldo+? WHERE id_persona=?";
            
            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setFloat(1, total);
            this.pst.setInt(2, id);
            
            respuesta = this.pst.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
}
