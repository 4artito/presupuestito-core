/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.costos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.costos.TipoDeCosto;
import presupuestito.core.model.dao.Conexion;

/**
 *
 * @author Luis
 */
public class TipoCostosDAO {
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo para crear un Tipo De Costo
     * @param tipoDeCosto objeto de Tipo de Costo con sus respectivos datos
     * @return respuesta
     */
    public int create(TipoDeCosto tipoDeCosto){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {            
            this.sql = "INSERT INTO TIPOS_DE_COSTOS (nombre, periodo) VALUES (?,?)";
            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setString(1, tipoDeCosto.getNombre().toUpperCase());
            this.pst.setFloat(2, tipoDeCosto.getPeriodo());            
            
            respuesta = this.pst.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    /**
     * Metodo para listar los tipos de costos
     * @return lista de tipos de costos
     */
    public List<TipoDeCosto> listar(){
        Connection cc = Conexion.getConexion();
        List<TipoDeCosto> listaTipoCosto = new ArrayList<>();
        TipoDeCosto tipoCosto = null;
        
        try {
            this.sql = "SELECT * FROM TIPOS_DE_COSTOS";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while (this.datos.next()){
                tipoCosto = new TipoDeCosto(
                        this.datos.getInt("id_tipo_de_costos"),
                        this.datos.getString("nombre"), 
                        this.datos.getFloat("periodo")
                );
                
                listaTipoCosto.add(tipoCosto);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaTipoCosto;
    }
    
    /**
     * Metodo para actualizar los tipos de costos
     * @param tipoDeCosto objeto con los datos a actualizar
     * @return respuesta
     */
    public int update(TipoDeCosto tipoDeCosto){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "UPDATE TIPOS_DE_COSTOS set nombre=?, periodo=? WHERE id_tipo_de_costos=?";
            
            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setString(1, tipoDeCosto.getNombre().toUpperCase());
            this.pst.setFloat(2, tipoDeCosto.getPeriodo());
            this.pst.setInt(3, tipoDeCosto.getId_tipo_costo());
            
            respuesta = this.pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    /**
     * Metodo que consulta la suma de los sueldos de los empleados activos,
     * cuenta cuantos empleados activos existen registrado y el periodo de los empleados
     * @return lista de float con [ suma de sueldos, cantidad de empleados, periodo ]
     */
    public List<Float> consultaSalarial(String categoria){
        List<Float> sueldoCant = new ArrayList<>();
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT sum(sueldo), count(fk_id_persona) " +
                        "FROM PERSONAS " +
                        "INNER JOIN " +
                        "EMPLEADOS ON PERSONAS.id_persona = EMPLEADOS.fk_id_persona " +
                        "WHERE PERSONAS.estado = 1 AND EMPLEADOS.categoria = '"+categoria+"';";
            
            this.st = cc.createStatement();
            
            this.datos = this.st.executeQuery(this.sql);
            
            if (this.datos.next()){                
                sueldoCant.add(this.datos.getFloat("sum(sueldo)"));
                sueldoCant.add(this.datos.getFloat("count(fk_id_persona)"));
            }
            
            this.sql = "SELECT DISTINCT periodo" +
                        " FROM TIPOS_DE_COSTOS" +
                        " WHERE nombre = 'SALARIAL'";
            
            this.datos = this.st.executeQuery(sql);
            
            if (this.datos.next()){
                sueldoCant.add(this.datos.getFloat("periodo"));
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        
        return sueldoCant;
    }
}
