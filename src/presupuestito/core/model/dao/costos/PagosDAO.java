/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.costos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.costos.Pago;
import presupuestito.core.model.dao.Conexion;

/**
 *
 * @author vhgav
 */
public class PagosDAO {

    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;

    public int create(Pago pagos) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "INSERT INTO PAGOS (fk_id_tipos_de_costos, total, fecha_de_pago) "
                    + "VALUES (?,?,?)";
            this.pst = cc.prepareStatement(this.sql);

            this.pst.setInt(1, pagos.getTipoDeCosto().getId_tipo_costo());
            this.pst.setFloat(2, pagos.getTotal());
            this.pst.setDate(3, Date.valueOf(pagos.getFechaDePago()));
            respuesta = this.pst.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        return respuesta;
    }

    public List<Pago> listar() {
        Pago pago = null;
        List<Pago> listaPago = new ArrayList<>();
        Connection cc = Conexion.getConexion();

        try {
            this.sql = "SELECT * FROM PAGOS INNER JOIN TIPOS_DE_COSTOS "
                    + "ON PAGOS.fk_id_tipos_de_costos = TIPOS_DE_COSTOS.id_tipo_de_costos";
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(sql);
            while (this.datos.next()) {
                pago = new Pago(this.datos.getInt("id_pago"), this.datos.getInt("fk_id_tipos_de_costos"),
                        this.datos.getString("nombre"), this.datos.getFloat("periodo"), this.datos.getFloat("total"),
                        LocalDate.parse(this.datos.getString("fecha_de_pago")));
                listaPago.add(pago);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaPago;
    }
    
    public List<Pago> filtrarLista(int id_tipoDeCosto, LocalDate fechaActual, LocalDate fechaLimite) {
        Pago pago = null;
        List<Pago> listaPago = new ArrayList<>();
        Connection cc = Conexion.getConexion();
        
        try {
            this.sql = "SELECT * FROM PAGOS INNER JOIN TIPOS_DE_COSTOS "
                    + "ON PAGOS.fk_id_tipos_de_costos = TIPOS_DE_COSTOS.id_tipo_de_costos "
                    + "WHERE fk_id_tipos_de_costos = '"+id_tipoDeCosto+"' "
                    + "AND PAGOS.fecha_de_pago BETWEEN '"+fechaLimite+"' AND '"+fechaActual+"'";
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(sql);
            while (this.datos.next()) {
                pago = new Pago(this.datos.getInt("id_pago"), this.datos.getInt("fk_id_tipos_de_costos"),
                        this.datos.getString("nombre"), this.datos.getFloat("periodo"), this.datos.getFloat("total"),
                        LocalDate.parse(this.datos.getString("fecha_de_pago")));
                listaPago.add(pago);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaPago;
    }
    
    public int eliminar(int id_pago) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        try {
            this.sql = "DELETE FROM PAGOS WHERE id_pago = ?";
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setInt(1, id_pago);
            respuesta = this.pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());      
        } finally{
            Conexion.closeConnection();
        }
        return respuesta;
    }
    
    public int actualizar(Pago pago) {
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        try {
            this.sql = "UPDATE PAGOS SET fk_id_tipos_de_costos=?, total=?,fecha_de_pago=? "
                    + "WHERE id_pago=?";
            this.pst = cc.prepareStatement(this.sql);
            this.pst.setInt(1, pago.getTipoDeCosto().getId_tipo_costo());
            this.pst.setFloat(2, pago.getTotal());
            this.pst.setDate(3, Date.valueOf(pago.getFechaDePago()));
            this.pst.setInt(4,pago.getId_pago());
            respuesta = this.pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally{
                Conexion.closeConnection();
        }
        return respuesta;
    }
}
