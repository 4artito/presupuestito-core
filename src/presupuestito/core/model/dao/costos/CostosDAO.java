/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.dao.costos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.model.costos.Costo;
import presupuestito.core.model.dao.Conexion;

/**
 *
 * @author Luis
 */
public class CostosDAO {
    private String sql;
    private Statement st;
    private ResultSet datos;
    private PreparedStatement pst;
    
    /**
     * Metodo que realiza la consulta a la base de datos para crear un costo
     * @param costo objeto de Costo con sus respectivos datos
     * @return respuesta
     */
    public int create(Costo costo){
        Connection cc = Conexion.getConexion();
        int respuesta = 0;
        
        try {
            this.sql = "INSERT INTO COSTOS (fk_id_tipos_de_costos, descripcion, valorPorHora) "
                    + "VALUES (?,?,?)";

            this.pst = cc.prepareStatement(this.sql);
            
            this.pst.setInt(1, costo.getTipoDeCosto().getId_tipo_costo());
            this.pst.setString(2, costo.getDescripcion());
            this.pst.setFloat(3, costo.getValorPorHora());
            
            respuesta = this.pst.executeUpdate();
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return respuesta;
    }
    
    /**
     * Metodo para listar los Costos
     * @return lista de costos
     */
    public List<Costo> listar() {
        Connection cc = Conexion.getConexion();
        List<Costo> listaCosto = new ArrayList<>();
        Costo costo = null;
        
        try {
            this.sql = "SELECT * FROM COSTOS "
                    + "INNER JOIN TIPOS_DE_COSTOS "
                    + "ON COSTOS.fk_id_tipos_de_costos = TIPOS_DE_COSTOS.id_tipo_de_costos";
            
            this.st = cc.createStatement();
            this.datos = this.st.executeQuery(this.sql);
            
            while (this.datos.next()){
                costo = new Costo(
                        this.datos.getInt("id_costo"), this.datos.getString("descripcion"), 
                        this.datos.getFloat("valorPorHora"), 
                        this.datos.getInt("id_tipo_de_costos"), this.datos.getString("nombre"),
                        this.datos.getFloat("periodo")
                );
                
                listaCosto.add(costo);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            Conexion.closeConnection();
        }
        
        return listaCosto;
    }    
}
