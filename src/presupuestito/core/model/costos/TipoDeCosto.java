/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.costos;

import java.util.Objects;

/**
 * Clase para crear los tipos de costo
 * Si el tipo de costo es SALARIAL la suma de los salarios de los empleados se obtiene
 * desde el Controlador de Empleados (CtrlEmpleadoSingleton.java) en la funcion sumarSalarios()
 * @author Luis
 */
public class TipoDeCosto {
    private int id_tipo_costo;
    private String nombre;
    private float periodo;

    public TipoDeCosto(){}
    
    /**
     * Constructor para listar tipos de costos
     * @param id_tipo_costo identificador del tipo de costo en la base de datos
     * @param nombre Nombre del tipo de costo
     * @param periodo tiempo expresador en hora
     */
    public TipoDeCosto(int id_tipo_costo, String nombre, float periodo) {
        this.id_tipo_costo = id_tipo_costo;
        this.nombre = nombre;
        this.periodo = periodo;
    }
    
    /**
     * Constructor para crear tipos de costos
     * @param nombre Nombre del tipo de costo
     * @param periodo tiempo expresador en hora
     */
    public TipoDeCosto(String nombre, float periodo) {
        this.nombre = nombre;
        this.periodo = periodo;
    }

    public int getId_tipo_costo() {
        return id_tipo_costo;
    }

    public void setId_tipo_costo(int id_tipo_costo) {
        this.id_tipo_costo = id_tipo_costo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPeriodo() {
        return periodo;
    }

    public void setPeriodo(float periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id_tipo_costo;
        hash = 53 * hash + Objects.hashCode(this.nombre);
        hash = 53 * hash + Float.floatToIntBits(this.periodo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoDeCosto other = (TipoDeCosto) obj;
        if (this.id_tipo_costo != other.id_tipo_costo) {
            return false;
        }
        if (Float.floatToIntBits(this.periodo) != Float.floatToIntBits(other.periodo)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoDeCosto{" + "id_tipo_costo=" + id_tipo_costo + ", nombre=" + nombre + ", periodo=" + periodo + '}';
    }

    
    
}
