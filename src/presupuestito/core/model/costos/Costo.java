/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.model.costos;

import java.util.Objects;

/**
 * Clase para crear Costos
 * @author Luis
 */
public class Costo {
    private int id_costo;
    private String descripcion;
    private float valorPorHora;
    private TipoDeCosto tipoDeCosto;

    /**
     * Constructor de los costos para listar
     * @param id_costo identificador del costo en la bd
     * @param descripcion breve descripcion del costo
     * @param valorPorHora valor expresado en horas del total del costo
     * @param id_tipo_costo identificador del tipo de costo en la bd
     * @param nombreTipoCosto nombre descriptivo del tipo de costo
     * @param periodoTipoCosto total del periodo del tipo de costo
     */
    public Costo(int id_costo, String descripcion, float valorPorHora, int id_tipo_costo, String nombreTipoCosto , float periodoTipoCosto) {
        this.id_costo = id_costo;
        this.descripcion = descripcion;
        this.valorPorHora = valorPorHora;
        //Datos del tipo de costo
        this.tipoDeCosto = new TipoDeCosto();
        this.tipoDeCosto.setId_tipo_costo(id_tipo_costo);
        this.tipoDeCosto.setNombre(nombreTipoCosto);
        this.tipoDeCosto.setPeriodo(periodoTipoCosto);
    }
    
    /**
     * Constructor para crear costos
     * @param descripcion breve descripcion del costo
     * @param valorPorHora valor expresado en horas del total del costo
     * @param id_tipo_costo identificador del tipo de costo en la bd
     */
    public Costo(String descripcion, float valorPorHora, int id_tipo_costo) {
        this.descripcion = descripcion;
        this.valorPorHora = valorPorHora;
        //Datos del tipo de costo
        this.tipoDeCosto = new TipoDeCosto();
        this.tipoDeCosto.setId_tipo_costo(id_tipo_costo);
    }
    
//    /**
//     * Metodo que calculara el valor por hora del costo
//     * El total es el del valor de las facturas o comprobantes del costos
//     * El periodo debe estar expresado en horas (calcular desde el FrontEnd)
//     * @return total/periodo
//     */
//    public float calcularValorPorHora(){
//        return this.total/this.periodo;
//    }

    public int getId_costo() {
        return id_costo;
    }

    public void setId_costo(int id_costo) {
        this.id_costo = id_costo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorPorHora() {
        return valorPorHora;
    }

    public void setValorPorHora(float valorPorHora) {
        this.valorPorHora = valorPorHora;
    }

    public TipoDeCosto getTipoDeCosto() {
        return tipoDeCosto;
    }

    public void setTipoDeCosto(TipoDeCosto tipoDeCosto) {
        this.tipoDeCosto = tipoDeCosto;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id_costo;
        hash = 67 * hash + Objects.hashCode(this.descripcion);
        hash = 67 * hash + Float.floatToIntBits(this.valorPorHora);
        hash = 67 * hash + Objects.hashCode(this.tipoDeCosto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Costo other = (Costo) obj;
        if (this.id_costo != other.id_costo) {
            return false;
        }
        if (Float.floatToIntBits(this.valorPorHora) != Float.floatToIntBits(other.valorPorHora)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.tipoDeCosto, other.tipoDeCosto)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Costo{" + "id_costo=" + id_costo + ", descripcion=" + descripcion + ", valorPorHora=" + valorPorHora + ", tipoDeCosto=" + tipoDeCosto + '}';
    }
    
}

/*
    Cuando el costo es salarial = el metodo tiene que sumar todos los salarios de los empleados y dividirlo (por periodo (44horas) multiplicado por la cantidad de empleados)
    Cuando el costo es servicios = el metodo tiene que dividir el total del costo del servicio por el periodo expresado en horas. La fecha es la de vencimiento
    Cuando el costo es un impuesto = si los impuestos se cargan individualmente es como los servicios. Y si se cargan todos juntos hay que unificar los periodos (como hay impuestos anuales hay que sumar el costo anual de todos los impuestos y se divide por el periodo anual expresado en horas)
    Cuando el costo es mantenimientos o insumos = consideremos el periodo anual se suman todos los costos del año (ultimos 12 meses (a conversar)) y se divide por el periodo anual expresado en horas. 
    De ser necesario incluir una actualizacion inflacionaria

    TIPO DE COSTO ADICIONAL : Horas Extras.
*/