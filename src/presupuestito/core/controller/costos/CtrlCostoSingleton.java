/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.costos;

import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.costos.Costo;
import presupuestito.core.model.costos.Pago;
import presupuestito.core.model.dao.costos.CostosDAO;
import presupuestito.core.model.dao.costos.TipoCostosDAO;

/**
 *
 * @author Luis
 */
public class CtrlCostoSingleton implements comprobable{
    private static CtrlCostoSingleton instance;
    private List<Costo> listaCosto = new ArrayList<>();
    private TipoCostosDAO tipoCostoDao = new TipoCostosDAO();
    private CostosDAO costoDao = new CostosDAO();

    /**
     * Metodo constructor privado por defecto
     */
    private CtrlCostoSingleton(){}

    /**
     * Metodo para obtener la instancia del controlador de costos
     * Regresa la instancia
     * @return instance
     */
    public static CtrlCostoSingleton getInstance() {
        if (instance == null) {
            instance = new CtrlCostoSingleton();
        }
        return instance;
    }

    /**
     * Metodo para crear un Costo
     * @param costo Objeto de tipo Costo con sus respectivos datos
     */
    public void crearCosto(Costo costo){
        int resultado = 0;
        resultado = this.costoDao.create(costo);
        comprobar(resultado);
    }
    
    /**
     * Metodo para listar los costos de la bd
     * @return la lista de costos
     */
    public List<Costo> listarCostos(){
        this.listaCosto = this.costoDao.listar();
        int resultado = this.listaCosto.size();
        comprobar(resultado);
        
        return this.listaCosto;
    }
    
    /**
     * metodo para obtener costo salarial horario
     * @param categoria: empleado,
     *                   empleado media jornada,
     *                   Iñaki,
     *                   padre
     * @return 
     */
    public float costoSalarialPorHoraEmpleados(String categoria) {
        List<Float> lf = new ArrayList<>();
        lf = this.tipoCostoDao.consultaSalarial(categoria.toLowerCase());
        
        float salarioTotal = lf.get(0);
        float cantidadEmpleado = lf.get(1);
        float periodo;
        
        //Parche para esquivar error IndexOutOfBoundsException, cuando no existe ningun periodo en la ddbb
        try {
            periodo = lf.get(2);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("No existe periodo salarial registrado en la base de datos " +e.getMessage());
            if ("empleado media jornada".equals(categoria.toLowerCase())){
                periodo = 22;   
            }
            else{
                periodo = 44;
            }
        }
        
        System.out.println("SalarioTotal: "+salarioTotal+" Cantidad Empleado: "+cantidadEmpleado+ " Periodo: "+periodo);

        return salarioTotal / (cantidadEmpleado * periodo);
    }
    
    /**
     * Metodo para calcular el costo horario de un tipo de Costo
     * @param listaPagos Lista con atributos necesarios para calcular el costo horario
     * @return  Costo Horario
     */
    public float calcularCostoHorario(List<Pago> listaPagos){
        float costoHorario = 0;
        
        for(Pago p : listaPagos){
            costoHorario += p.getTotal();
        }
        
        try {
            costoHorario /= 2288;
        } catch (ArithmeticException ae){
            System.out.println("No se puede dividir por 0 " + ae.getMessage());
        }
        
        return costoHorario;
    }
    
    /**
     * Metodo para sumar todos los costos horarios
     * @param listaDeCostosHorarios Lista de Floats que contiene los diferentes costos horarios
     * @return suma de los costos horarios
     */
    public float sumarTodosCostosHorarios(List<Float> listaDeCostosHorarios) {
        float sumaCostoHorario = 0;
        for (Float c : listaDeCostosHorarios){
            sumaCostoHorario += c;
        }
        return sumaCostoHorario;
    }
  
    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
}
