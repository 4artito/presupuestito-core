/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.costos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.costos.Pago;
import presupuestito.core.model.dao.costos.PagosDAO;

/**
 * clase para crear, modificar y listara los pagos
 *
 * @author vhgav
 */
public class CtrlPagosSingleton implements comprobable{

    private static CtrlPagosSingleton instance;
    private PagosDAO pagosDao = new PagosDAO();
    List<Pago> listaPagos = new ArrayList<>();
    /**
     * constructor
     */
    private CtrlPagosSingleton() {
    }

    /**
     *  método que verifica que la instancia sea única
     * @return
     */
    public static CtrlPagosSingleton getInstance() {
        if (instance == null) {
            instance = new CtrlPagosSingleton();
        }
        return instance;
    }
   
    /**
     * método para crear un pago
     * @param pago 
     */
    public void crearPagos(Pago pago){
        int resultado = 0;
        System.out.println(pago);
        resultado = this.pagosDao.create(pago);
        comprobar(resultado);
    }
 
    @Override
    public void comprobar(int resultado) {
         if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
    
    /**
     * método para listar los pagos
     * @return 
     */
    public List<Pago> listarPagos(){
        this.listaPagos = this.pagosDao.listar();
        comprobar(this.listaPagos.size());
        return this.listaPagos;
    }
    
    /**
     * Metodo para filtrar los pagos a partir de un tipo de costo en especifico 
     * @param fk_tipo_de_costo identificador del tipo de costo 
     * @param fechaActual fecha del dia de la fecha
     * @return Lista de pagos filtrada por un tipo de costo
     */
    public List<Pago> filtrarPagos(int fk_tipo_de_costo, LocalDate fechaActual) {
        LocalDate fechaLimite = restarFechaActual(fechaActual);
        this.listaPagos = this.pagosDao.filtrarLista(fk_tipo_de_costo, fechaActual, fechaLimite);
        comprobar(this.listaPagos.size());
        return this.listaPagos;
    }
    
    private LocalDate restarFechaActual(LocalDate fechaActual) {
        return fechaActual.minusYears(1);
    }
    
    /**
     * método para modificar datos de un pago
     * @param pago 
     */
    public void actualizarPago(Pago pago){
        int resultado = 0;
        resultado = this.pagosDao.actualizar(pago);
        comprobar(resultado);
    } 
    
    /**
     * método para eliminar un pago
     * @param idPago 
     */
    public void eliminarPago(int idPago){
        int resultado = 0;
        resultado = this.pagosDao.eliminar(idPago);
        comprobar (resultado);
    }
}
