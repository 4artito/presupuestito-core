/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.costos;

import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.costos.TipoDeCosto;
import presupuestito.core.model.dao.costos.TipoCostosDAO;

/**
 * Clase para crear Tipos de Costos
 * @author Luis
 */
public class CtrlTipoDeCostoSingleton implements comprobable{
    private static CtrlTipoDeCostoSingleton instance;
    private List<TipoDeCosto> listaTipoCosto = new ArrayList<>();
    private TipoCostosDAO tipoCostoDao = new TipoCostosDAO();
    
    /**
     * Metodo constructo privado por defecto
     */
    private CtrlTipoDeCostoSingleton(){}

    /**
     * Metodo para obtener la instancia del controlador de clientes
     * Regresa la instancia
     * @return instance
     */
    public static CtrlTipoDeCostoSingleton getInstance() {
        if (instance == null) {
            instance = new CtrlTipoDeCostoSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para crear un tipo de costo
     * @param tipoDeCosto objeto de TipoDeCosto con el nombre, total, periodo y fecha.
     */
    public void crearTipoCosto(TipoDeCosto tipoDeCosto){
        int resultado = this.tipoCostoDao.create(tipoDeCosto);
        comprobar(resultado);
    }
    
    /**
     * Metodo para listar los tipos de costos
     * @return listaTipoCosto
     */
    public List<TipoDeCosto> listarTipoCosto(){
        int resultado = 0; 
        this.listaTipoCosto = this.tipoCostoDao.listar();
        resultado = this.listaTipoCosto.size();
        comprobar(resultado);
        
        return this.listaTipoCosto;
    }
    
    /**
     * Actualiza el nombre del tipo de costo
     * @param tipoDeCosto Objeto de tipoDeCosto con sus respectivos datos
     */
    public void actualizarTipoDeCosto(TipoDeCosto tipoDeCosto){
        int resultado = 0;
        resultado = this.tipoCostoDao.update(tipoDeCosto);
        comprobar(resultado);
    }

    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
}
