/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.personas;

import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.ABLM;
import presupuestito.core.model.personas.Proveedor;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.dao.personas.PersonaDAO;
import presupuestito.core.model.dao.personas.ProveedorDAO;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los atributos y metodos de la controladora de proveedores
 * @author Luis
 */
public class CtrlProveedorSingleton implements comprobable, ABLM{
    private static CtrlProveedorSingleton instance;
    private List<Proveedor> listaProveedor = new ArrayList<>();
    private ProveedorDAO proveedorDao = new ProveedorDAO();
    
    /**
     * Metodo constructor privado por defecto
     */
    private CtrlProveedorSingleton(){}
    
    /**
     * Metodo para obtener la instancia del controlador de proveedores
     * Regresa la instancia
     * @return instance
     */
    public static CtrlProveedorSingleton getInstance(){
        if (instance == null) {
            instance = new CtrlProveedorSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para crear un proveedor
     * @param proveedor Proveedor que se va a crear
     */
    @Override
    public void crear(Persona proveedor){
        if(proveedor instanceof Proveedor){
            int resultado = this.proveedorDao.create(proveedor);

            comprobar(resultado);
        }
    }
    
    /**
     * Metodo que regresa el listado de proveedores que se encuentran "activos"
     * en la base de datos
     * @param estado
     * @param limit
     * @param offset
     * @return listaProveedor
     */
    @Override
    public List<Proveedor> listar(int estado, int limit, int offset){
        
        this.listaProveedor = this.proveedorDao.listar(estado, limit, offset);
        
        comprobar(this.listaProveedor.size());
        
        return this.listaProveedor;
    }
    
    /**
     * Metodo para actualizar un proveedor
     * @param proveedor Proveedor que se a actualizar
     */
    @Override
    public void actualizar(Persona proveedor){
        if(proveedor instanceof Proveedor){
            int resultado = this.proveedorDao.update(proveedor);

            comprobar(resultado);
        }
    }
    
    /**
     * Metodo para desactivar un proveedor
     * @param id Identificador de un proveedor
     */
    @Override
    public void darDeBaja(int id){
        
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.borradoLogico(id);
        
        comprobar(resultado);
    }
    
    /**
     * Metodo para activar un proveedor
     * @param id Identificador de un proveedor
     */
    @Override
    public void darDeAlta(int id){
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.altaLogica(id);
        
        comprobar(resultado);
    }

    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
}
