/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.personas;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import presupuestito.core.controller.interfaces.ABLM;
import presupuestito.core.model.personas.Empleado;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.dao.personas.EmpleadoDAO;
import presupuestito.core.model.dao.personas.PersonaDAO;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los atributos y metodos de la controladora de empleados
 * @author Luis
 * @version 1.0
 */
public final class CtrlEmpleadoSingleton implements comprobable, ABLM{
    private static CtrlEmpleadoSingleton instance;
    private List<Empleado> listaEmpleado = new ArrayList<>();
    private EmpleadoDAO empleadoDao = new EmpleadoDAO();
    
    /**
     * Metodo constructor privado por defecto
     */
    private CtrlEmpleadoSingleton(){}
    
    /**
     * Metodo para obtener la instancia del controlador de empleados
     * Regresa la instancia
     * @return instance
     */
    public static CtrlEmpleadoSingleton getInstance(){
        if (instance == null) {
            instance = new CtrlEmpleadoSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para crear un empleado
     * @param e Empleado que se va a crear
     */
    @Override
    public void crear(Persona e){
        if(e instanceof Empleado){
            int resultado = this.empleadoDao.create(e);

            comprobar(resultado);
        } else {
            JOptionPane.showMessageDialog(null, "La persona no es un empleado");
        }
    }
    
    /**
     * Metodo que regresa el listado de empleados que se encuentran "activos"
     * en la base de datos
     * @param estado Indica el estado en que se encuentra el Empleado en la base de datos:
     *                  0 = Inactivo  1 = Activo
     * @param limit  Indica la cantidad de filas que traera de Empleados la consulta a la base de datos
     * @param offset Indica desde donde empieza a tomar los datos que traera la consulta, tiene que ser incremental
     * @return listaEmpleado
     */
    @Override
    public List<Empleado> listar(int estado, int limit, int offset){
        this.listaEmpleado = this.empleadoDao.listar(estado, limit, offset);
        
        System.out.println("limit: "+limit+ " offset: "+offset);
        
        comprobar(this.listaEmpleado.size());
        
        return this.listaEmpleado;
    }
    
    /**
     * Metodo para actualizar un empleado
     * @param e Empleado a actualizar
     */
    @Override
    public void actualizar(Persona e){
        if(e instanceof Empleado) {
            int resultado = this.empleadoDao.update(e);

            comprobar(resultado);
        } else {
            JOptionPane.showMessageDialog(null, "La persona no es un empleado");
        }
    }
    
    /**
     * Metodo para desactivar un empleado
     * @param id Identificador del empleado
     */
    @Override
    public void darDeBaja(int id){
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.borradoLogico(id);
        
        comprobar(resultado);
    }
    
    /**
     * Metodo para activar un empleado
     * @param id Identificador del empleado
     */
    @Override
    public void darDeAlta(int id){
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.altaLogica(id);
        
        comprobar(resultado);
    }
    
    /**
     * Metodo para obtener la suma de los sueldos de los empleados activos
     * @return suma de los sueldos
     */
    public float sumarSueldos(){
        float suma = 0;
        suma = this.empleadoDao.sumarSueldos();
        return suma;
    }

    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
}
