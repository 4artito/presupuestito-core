/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.personas.transacciones;

import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.personas.transacciones.Compra;
import presupuestito.core.model.dao.transacciones.ComprasDAO;
import presupuestito.core.model.dao.transacciones.TransaccionDAO;

/**
 *
 * @author Luis
 */
public class CtrlComprasSingleton implements comprobable{

    private static CtrlComprasSingleton instance;
    private List<Compra> listaCompras = new ArrayList<>();
    private ComprasDAO compraDao = new ComprasDAO();
    private TransaccionDAO transaccionDao = new TransaccionDAO();

    /**
     * Metodo constructo privado por defecto
     */
    private CtrlComprasSingleton(){}

    /**
     * Metodo para obtener la instancia del controlador de compras
     * Regresa la instancia
     * @return instance
     */
    public static CtrlComprasSingleton getInstance() {
        if (instance == null) {
            instance = new CtrlComprasSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para crear una Compra
     * @param compra objeto que contiene todos los atributos de la Compra
     */
    public void crearCompra(Compra compra){
        int resultado = this.compraDao.create(compra);
        
        comprobar(resultado);
        
        updateSaldoProveedor(compra.getPersona().getId_persona(), compra.getTotal());
    }
    
    /**
     * Metodo para obtener una lista de registro de las compras
     * @param id identificador de la persona que se quiere obtener la lista de compras
     * @param estado estado que se encuentra la compra: 1=activo, 0=inactivo
     * @return 
     */
    public List<Compra> registros(int id, int estado){
        this.listaCompras = compraDao.listar(id, estado);
        
        comprobar(this.listaCompras.size());
        
        return this.listaCompras;
    }

    /**
     * Metodo para modificar una compra
     * @param compraModificada objeto con los datos nuevos de la comprar
     * @param compraEquivocada objeto de la compra que se quiere modificar
     */
    public void modificarCompra(Compra compraModificada, Compra compraEquivocada){
        
        if(compraModificada.getPersona().getId_persona() != compraEquivocada.getPersona().getId_persona()){
            
            float totalEquivocado = compraEquivocada.getTotal();
            updateSaldoProveedor(compraEquivocada.getPersona().getId_persona(), -totalEquivocado);
            
            updateSaldoProveedor(compraModificada.getPersona().getId_persona(), compraModificada.getTotal());
            ABM(compraModificada, compraEquivocada);
        } else {
            if(compraModificada.getTotal() != compraEquivocada.getTotal()){
                updateSaldoProveedor(
                        compraModificada.getPersona().getId_persona(), 
                        compraModificada.getTotal() - compraEquivocada.getTotal()
                );
                ABM(compraModificada, compraEquivocada);
            } else {
                ABM(compraModificada, compraEquivocada);
            }
        }
    }
    
    /**
     * Metodo privado para hacer la creacion, modificacion y el borrado logico de las compras
     * @param compraModificada objeto con los datos nuevos de la comprar
     * @param compraEquivocada objeto de la compra que se quiere modificar 
     */
    private void ABM(Compra compraModificada, Compra compraEquivocada){
        int resultado = 0;
        resultado = this.compraDao.create(compraModificada);
        comprobar(resultado);
        
        resultado = 0;
        resultado = this.compraDao.modificarCompra(compraEquivocada);
        comprobar(resultado);
        
        resultado = 0;
        resultado = this.compraDao.borradoLogicoCompra(compraEquivocada.getNumero_factura());
        comprobar(resultado);
    }
    
    /**
     * Metodo privado para actualizar el saldo del proveedor
     * @param id identificador de la persona
     * @param saldo saldo de la persona a actualizar
     */
    private void updateSaldoProveedor(int id, float saldo){
        int resultado = 0;
        resultado = this.transaccionDao.updateSaldo(id, saldo);
        comprobar(resultado);
    }
    
    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }
    
}
