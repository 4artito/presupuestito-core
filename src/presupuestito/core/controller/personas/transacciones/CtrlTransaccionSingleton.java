/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.personas.transacciones;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.personas.transacciones.Transaccion;
import presupuestito.core.model.dao.transacciones.TransaccionDAO;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los atributos y metodos de la controladora de Transacciones
 * @author Luis
 * @version 1.0
 */
public final class CtrlTransaccionSingleton implements comprobable{
    
    private static CtrlTransaccionSingleton instance;
    private TransaccionDAO transaccionDao = new TransaccionDAO();
    
    /**
     * Metodo constructor privado por defecto
     */
    private CtrlTransaccionSingleton() {}
    
    /**
     * Metodo para obtener la instancia del controlador de empleados
     * Regresa la instancia
     * @return instance
     */
    public static CtrlTransaccionSingleton getInstance(){
        if(instance == null){
            instance = new CtrlTransaccionSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para realizar una transaccion de la persona:
     *              del cliente hacia la empresa, 
     *              o de la empresa hacia el proveedor o a el empleado.
     * Para el proveedor: El saldo de la persona se acredita cuando se registra un total de una factura.
     * Para el empleado: El saldo salarial se actualiza automaticamente en la base de datos.
     * Para el cliente: El saldo se registra al aprobarse un presupuesto (multiplicado * -1). 
     * @param persona Es tipo de persona puede ser (Cliente, Proveedor, Empleado)
     * @param monto Es la cantidad del pago a cuenta.          
     */
    public void transaccion(Persona persona, float monto) {
        System.out.println(monto);
//        if(e instanceof Cliente){
//            System.out.println("hola");
//            monto *= -1;
//        }
        System.out.println(monto);
        float saldo = persona.getSaldo() - monto;
        persona.setSaldo(saldo);
        
        int resultado2 = this.transaccionDao.updateSaldo(persona.getId_persona(), persona.getSaldo());
        comprobar(resultado2);
        
        String fechaActual = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        int resultado = this.transaccionDao.transaccionEmpleado(persona.getId_persona(), monto, fechaActual);
        comprobar(resultado);
    }

    /**
     * Metodo para obtener la lista de las transacciones de cada persona
     * @param id identificador de la persona
     * @return lista de transaccion
     */
    public List<Transaccion> listarTransacciones(int id) {
        List<Transaccion> listaTransaccion = new ArrayList<>();
        
        listaTransaccion = this.transaccionDao.listarTransaccionPersonas(id);
        comprobar(listaTransaccion.size());
        return listaTransaccion;
    }

    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }

    
    
}
