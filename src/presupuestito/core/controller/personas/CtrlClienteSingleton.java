/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.personas;

import java.util.ArrayList;
import java.util.List;
import presupuestito.core.controller.interfaces.ABLM;
import presupuestito.core.model.personas.Cliente;
import presupuestito.core.controller.interfaces.comprobable;
import presupuestito.core.model.dao.personas.ClienteDAO;
import presupuestito.core.model.dao.personas.PersonaDAO;
import presupuestito.core.model.personas.Persona;

/**
 * Esta clase contiene los atributos y metodos de la controladora de clientes
 * @author Luis
 * @version 1.0
 */
public final class CtrlClienteSingleton implements comprobable, ABLM{

    private static CtrlClienteSingleton instance;
    private List<Cliente> listaCliente = new ArrayList<>();
    private ClienteDAO clienteDao = new ClienteDAO();

    /**
     * Metodo constructo privado por defecto
     */
    private CtrlClienteSingleton(){}

    /**
     * Metodo para obtener la instancia del controlador de clientes
     * Regresa la instancia
     * @return instance
     */
    public static CtrlClienteSingleton getInstance() {
        if (instance == null) {
            instance = new CtrlClienteSingleton();
        }
        return instance;
    }
    
    /**
     * Metodo para crear un cliente
     * @param cliente Cliente que se va a crear
     */
    @Override
    public void crear(Persona cliente){
        if(cliente instanceof Cliente){
            int resultado = clienteDao.create(cliente);

            comprobar(resultado);
        }
    }
    
    /**
     * Metodo que regresa el listado de clientes que se encuentran "activos"
     * en la base de datos
     * @param estado Indica el estado en que se encuentra el Cliente en la base de datos:
     *                  0 = Inactivo  1 = Activo
     * @param limit  Indica la cantidad de filas que traera del Cliente la consulta a la base de datos
     * @param offset Indica desde donde empieza a tomar los datos que traera la consulta, tiene que ser incremental
     * @return listaCliente
     */
    @Override
    public List<Cliente> listar(int estado, int limit, int offset){
        this.listaCliente = clienteDao.listar(estado, limit, offset);
        
        comprobar(this.listaCliente.size());
        
        return this.listaCliente;
    }
    
    /**
     * Metodo para actualizar un cliente
     * @param cliente Cliente que se va a actualizar
     */
    @Override
    public void actualizar(Persona cliente){
        if(cliente instanceof Cliente){
            int resultado = clienteDao.update(cliente);

            comprobar(resultado);
        }
    }
    
    /**
     * Metodo para desactivar un cliente
     * @param id Identificador del cliente
     */
    @Override
    public void darDeBaja(int id){
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.borradoLogico(id);
        
        comprobar(resultado);
    }
    
    /**
     * Metodo para activar un cliente
     * @param id Identificador del cliente
     */
    @Override
    public void darDeAlta(int id){
        PersonaDAO personaDao = new PersonaDAO();
        
        int resultado = personaDao.altaLogica(id);
        
        comprobar(resultado);
    }
    
    //FUTURO METODO A IMPLEMENTAR CUANDO EXISTA LA FUNCION DE GENERAR Y LISTAR PRESUPUESTOS
    public void getPresupuestosOf(Cliente c){
        
    }

    @Override
    public void comprobar(int resultado) {
        if (resultado > 0) {
            System.out.println("El resultado fue exitoso");
        } else {
            System.out.println("Error al realizar la consulta");
        }
    }


}
