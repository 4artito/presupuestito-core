/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import presupuestito.core.controller.personas.CtrlClienteSingleton;
import presupuestito.core.model.personas.Cliente;
import presupuestito.core.view.VentanaCliente;

/**
 *
 * @author Luis
 */
public class CtrlCliente implements ActionListener{
    private VentanaCliente vistaCliente = new VentanaCliente();
    
    private CtrlClienteSingleton ctrlClienteS;
    
    public CtrlCliente(VentanaCliente vistaCliente){
        this.vistaCliente = vistaCliente;
        this.ctrlClienteS = CtrlClienteSingleton.getInstance();
        vistaCliente.btnGuardar.addActionListener(this);
        vistaCliente.btnListar.addActionListener(this);
        vistaCliente.btnModificar.addActionListener(this);
        vistaCliente.btnBorrar.addActionListener(this);
        vistaCliente.btnGuardaModificar.addActionListener(this);
    }

    public void llenarTabla(JTable tablaE){
        DefaultTableModel modeloT = new DefaultTableModel();
        tablaE.setModel(modeloT);
        
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("APODO");
        modeloT.addColumn("APELLIDO");
        modeloT.addColumn("DNI/CUIT");
        modeloT.addColumn("SALDO");
        modeloT.addColumn("DIRECCION");
        modeloT.addColumn("MAIL");
        modeloT.addColumn("TELEFONO");
        modeloT.addColumn("DESCRIPCION");
        
        
        modeloT.addColumn("id_cliente");
        modeloT.addColumn("id_persona");
        
        Object[] columna = new Object[11];
        
        //almacena el numero de registro que se recupera de la base de datos
//        int numRegistro = ctrlClienteS.listarClientesActivos().size();
//        
//        for (int i = 0; i < numRegistro; i++){
//            columna[0] = ctrlClienteS.listarClientesActivos().get(i).getNombre();
//            columna[1] = ctrlClienteS.listarClientesActivos().get(i).getApodo();
//            columna[2] = ctrlClienteS.listarClientesActivos().get(i).getApellido();
//            columna[3] = ctrlClienteS.listarClientesActivos().get(i).getDni_cuit();
//            columna[4] = ctrlClienteS.listarClientesActivos().get(i).getSaldo();
//            columna[5] = ctrlClienteS.listarClientesActivos().get(i).getDireccion();
//            columna[6] = ctrlClienteS.listarClientesActivos().get(i).getMail();
//            columna[7] = ctrlClienteS.listarClientesActivos().get(i).getTelefono();
//            columna[8] = ctrlClienteS.listarClientesActivos().get(i).getDescripcion();
//            columna[9] = ctrlClienteS.listarClientesActivos().get(i).getId_cliente();
//            vistaCliente.jtClientes.getColumnModel().getColumn(9).setMinWidth(0);
//            vistaCliente.jtClientes.getColumnModel().getColumn(9).setMaxWidth(0);
//            columna[10] = ctrlClienteS.listarClientesActivos().get(i).getId_persona();
//            vistaCliente.jtClientes.getColumnModel().getColumn(10).setMinWidth(0);
//            vistaCliente.jtClientes.getColumnModel().getColumn(10).setMaxWidth(0);
//            
//            
//            modeloT.addRow(columna);
//        }
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vistaCliente.btnGuardar){
            String nombre = vistaCliente.txtNombre.getText();
            String apodo = vistaCliente.txtApodo.getText();
            String apellido = vistaCliente.txtApellido.getText();
            String dni_cuit = vistaCliente.txtDni.getText();
            float saldo = Float.parseFloat(vistaCliente.txtSaldo.getText());
            String direccion = vistaCliente.txtDireccion.getText();
            String mail = vistaCliente.txtMail.getText();
            String telefono = vistaCliente.txtTelefono.getText();
            String descripcion = vistaCliente.txtDescripcion.getText();
            
            
            Cliente cli = new Cliente(
                    nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, descripcion
            );
            
//            ctrlClienteS.crearCliente(cli);
        }
        
        if(e.getSource() == vistaCliente.btnListar){
            llenarTabla(vistaCliente.jtClientes);
        }
        
        if(e.getSource() == vistaCliente.btnModificar){
            vistaCliente.txtNombre.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 0).toString());
            vistaCliente.txtApodo.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 1).toString());
            vistaCliente.txtApellido.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 2).toString());
            vistaCliente.txtDni.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 3).toString());
            vistaCliente.txtSaldo.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 4).toString());
            vistaCliente.txtDireccion.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 5).toString());
            vistaCliente.txtMail.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 6).toString());
            vistaCliente.txtTelefono.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 7).toString());
            vistaCliente.txtDescripcion.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 8).toString());
            
            vistaCliente.txtIdCliente.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 9).toString());
            vistaCliente.txtIdPersona.setText(vistaCliente.jtClientes.getValueAt(vistaCliente.jtClientes.getSelectedRow(), 10).toString());
        }
        
        if(e.getSource() == vistaCliente.btnGuardaModificar){
            String nombre = vistaCliente.txtNombre.getText();
            String apodo = vistaCliente.txtApodo.getText();
            String apellido = vistaCliente.txtApellido.getText();
            String dni_cuit = vistaCliente.txtDni.getText();
            float saldo = Float.parseFloat(vistaCliente.txtSaldo.getText());
            String direccion = vistaCliente.txtDireccion.getText();
            String mail = vistaCliente.txtMail.getText();
            String telefono = vistaCliente.txtTelefono.getText();
            String descripcion = vistaCliente.txtDescripcion.getText();
            int id_cliente = Integer.parseInt(vistaCliente.txtIdCliente.getText());
            int id_persona = Integer.parseInt(vistaCliente.txtIdPersona.getText());
            
            Cliente cli = new Cliente(
                    id_persona, nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, descripcion, id_cliente
            );
            
//            ctrlClienteS.actualizarCliente(cli);
        }
        
        if(e.getSource() == vistaCliente.btnBorrar){
            int id_persona = Integer.parseInt(vistaCliente.txtIdPersona.getText());
            
//            ctrlClienteS.borrarCliente(id_persona);
        }
    }
}
