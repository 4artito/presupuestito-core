/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.view;
import presupuestito.core.model.personas.Empleado;
import presupuestito.core.view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import presupuestito.core.controller.personas.CtrlEmpleadoSingleton;

/**
 * 
 * @author Luis
 */
public class CtrlEmpleado implements ActionListener{
    
    VentanaEmpleado vistaEmpleado = new VentanaEmpleado();
    private CtrlEmpleadoSingleton ctrlEmpleadoS;
    
    public CtrlEmpleado(VentanaEmpleado vistaEmpleado){
        this.vistaEmpleado = vistaEmpleado;
        ctrlEmpleadoS = CtrlEmpleadoSingleton.getInstance();
        vistaEmpleado.btnGuardar.addActionListener(this);
        vistaEmpleado.btnListar.addActionListener(this);
        vistaEmpleado.btnListarBajas.addActionListener(this);
        vistaEmpleado.btnModificar.addActionListener(this);
        vistaEmpleado.btnGuardaModificar.addActionListener(this);
        vistaEmpleado.btnBorrar.addActionListener(this);
    }
    
    //Funcion de Testeo
    public CtrlEmpleado(){}
    
    //Funcion de Testeo
//    public void pagoSalarial(Empleado e){
//        try {
//            ctrlEmpleadoS = CtrlEmpleadoSingleton.getInstance();
//            ctrlEmpleadoS.pagoSalarial(e, 10000);
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
//      
//    }
//    
//    //Funcion de Testeo
//    public void listarTransacciones(){
//        ctrlEmpleadoS = CtrlEmpleadoSingleton.getInstance();
//        List<Transaccion> listaTransaccion = new ArrayList<>();
//        listaTransaccion = ctrlEmpleadoS.listarTransacciones();
//        
//        listaTransaccion.forEach(System.out::println);
//    }
    
    //Funcion de testeo
    int limit = 10;
    int offset = 0;
    public void empleadosAlta(){
        ctrlEmpleadoS = CtrlEmpleadoSingleton.getInstance();
        List<Empleado> listaEmpleados = new ArrayList<>();
        listaEmpleados = ctrlEmpleadoS.listar(1, limit, offset);
        this.offset += limit;
        listaEmpleados.forEach(System.out::println);
    }

    public void llenarTablaAlta(JTable tablaE){
        DefaultTableModel modeloT = new DefaultTableModel();
        tablaE.setModel(modeloT);
        
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("APODO");
        modeloT.addColumn("APELLIDO");
        modeloT.addColumn("DNI");
        modeloT.addColumn("SALDO");
        modeloT.addColumn("DIRECCION");
        modeloT.addColumn("MAIL");
        modeloT.addColumn("TELEFONO");
        modeloT.addColumn("DESCRIPCION");
        modeloT.addColumn("SUELDO");
        modeloT.addColumn("CATEGORIA");
        modeloT.addColumn("PERIODO");
        
        modeloT.addColumn("id_cliente");
        modeloT.addColumn("id_persona");
        
        Object[] columna = new Object[14];
        
        //almacena el numero de registro que se recupera de la base de datos
//        int numRegistro = ctrlEmpleadoS.listarEmpleadosActivos(10).size();
//        
//        try {
//            for (int i = 0; i < numRegistro; i++){
//                columna[0] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getNombre();
//                columna[1] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getApodo();
//                columna[2] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getApellido();
//                columna[3] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getDni_cuit();
//                columna[4] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getSaldo();
//                columna[5] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getDireccion();
//                columna[6] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getMail();
//                columna[7] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getTelefono();
//                columna[8] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getDescripcion();
//                columna[9] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getSueldo();
//                columna[10] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getCategoria();
//                columna[11] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getPeriodo();
//                columna[12] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getId_empleado();
//                vistaEmpleado.jtEmpleados.getColumnModel().getColumn(12).setMinWidth(0);
//                vistaEmpleado.jtEmpleados.getColumnModel().getColumn(12).setMaxWidth(0);
//                columna[13] = ctrlEmpleadoS.listarEmpleadosActivos(10).get(i).getId_persona();
//                vistaEmpleado.jtEmpleados.getColumnModel().getColumn(13).setMinWidth(0);
//                vistaEmpleado.jtEmpleados.getColumnModel().getColumn(13).setMaxWidth(0);
//
//                modeloT.addRow(columna);
//        }
//        } catch (IndexOutOfBoundsException e) {
//            System.out.println(e.getMessage());
//        }
//        
    }
    
//    public void llenarTablaBaja(JTable tablaE){
//        DefaultTableModel modeloT = new DefaultTableModel();
//        tablaE.setModel(modeloT);
//        
//        modeloT.addColumn("NOMBRE");
//        modeloT.addColumn("APODO");
//        modeloT.addColumn("APELLIDO");
//        modeloT.addColumn("DNI");
//        modeloT.addColumn("SALDO");
//        modeloT.addColumn("DIRECCION");
//        modeloT.addColumn("MAIL");
//        modeloT.addColumn("TELEFONO");
//        modeloT.addColumn("DESCRIPCION");
//        modeloT.addColumn("SUELDO");
//        modeloT.addColumn("CATEGORIA");
//        modeloT.addColumn("PERIODO");
//        
//        modeloT.addColumn("id_cliente");
//        modeloT.addColumn("id_persona");
//        
//        Object[] columna = new Object[14];
//        
//        //almacena el numero de registro que se recupera de la base de datos
//        int numRegistro = ctrlEmpleadoS.listarEmpleadosDesactivos().size();
//        
//        for (int i = 0; i < numRegistro; i++){
//            columna[0] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getNombre();
//            columna[1] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getApodo();
//            columna[2] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getApellido();
//            columna[3] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getDni_cuit();
//            columna[4] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getSaldo();
//            columna[5] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getDireccion();
//            columna[6] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getMail();
//            columna[7] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getTelefono();
//            columna[8] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getDescripcion();
//            columna[9] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getSueldo();
//            columna[10] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getCategoria();
//            columna[11] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getPeriodo();
//            columna[12] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getId_empleado();
//            vistaEmpleado.jtEmpleados.getColumnModel().getColumn(12).setMinWidth(0);
//            vistaEmpleado.jtEmpleados.getColumnModel().getColumn(12).setMaxWidth(0);
//            columna[13] = ctrlEmpleadoS.listarEmpleadosDesactivos().get(i).getId_persona();
//            vistaEmpleado.jtEmpleados.getColumnModel().getColumn(13).setMinWidth(0);
//            vistaEmpleado.jtEmpleados.getColumnModel().getColumn(13).setMaxWidth(0);
//            
//            modeloT.addRow(columna);
//        }
//    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vistaEmpleado.btnGuardar){
            String nombre = vistaEmpleado.txtNombre.getText();
            String apodo = vistaEmpleado.txtApodo.getText();
            String apellido = vistaEmpleado.txtApellido.getText();
            String dni_cuit = vistaEmpleado.txtDni.getText();
            float saldo = Float.parseFloat(vistaEmpleado.txtSaldo.getText());
            String direccion = vistaEmpleado.txtDireccion.getText();
            String mail = vistaEmpleado.txtMail.getText();
            String telefono = vistaEmpleado.txtTelefono.getText();
            String descripcion = vistaEmpleado.txtDescripcion.getText();
            float sueldo = Float.parseFloat(vistaEmpleado.txtSueldo.getText());
            String categoria = vistaEmpleado.txtCategoria.getText();
            float periodo = Float.parseFloat(vistaEmpleado.txtPeriodo.getText());
            
            Empleado emp = new Empleado(
                    nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, 
                    descripcion, sueldo, categoria, periodo
            );
            
            ctrlEmpleadoS.crear(emp);
        }
        
        if(e.getSource() == vistaEmpleado.btnListar){
            //llenarTablaAlta(vistaEmpleado.jtEmpleados);
            empleadosAlta();
        }
        
//        if(e.getSource() == vistaEmpleado.btnListarBajas){
//            llenarTablaBaja(vistaEmpleado.jtEmpleados);
//        }
        
        if(e.getSource() == vistaEmpleado.btnModificar){
            vistaEmpleado.txtNombre.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 0).toString());
            vistaEmpleado.txtApodo.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 1).toString());
            vistaEmpleado.txtApellido.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 2).toString());
            vistaEmpleado.txtDni.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 3).toString());
            vistaEmpleado.txtSaldo.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 4).toString());
            vistaEmpleado.txtDireccion.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 5).toString());
            vistaEmpleado.txtMail.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 6).toString());
            vistaEmpleado.txtTelefono.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 7).toString());
            vistaEmpleado.txtDescripcion.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 8).toString());
            vistaEmpleado.txtSueldo.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 9).toString());
            vistaEmpleado.txtCategoria.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 10).toString());
            vistaEmpleado.txtPeriodo.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 11).toString());
            vistaEmpleado.txtIdEmpleado.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 12).toString());
            vistaEmpleado.txtIdPersona.setText(vistaEmpleado.jtEmpleados.getValueAt(vistaEmpleado.jtEmpleados.getSelectedRow(), 13).toString());
        }
        
        if(e.getSource() == vistaEmpleado.btnGuardaModificar){
            String nombre = vistaEmpleado.txtNombre.getText();
            String apodo = vistaEmpleado.txtApodo.getText();
            String apellido = vistaEmpleado.txtApellido.getText();
            String dni_cuit = vistaEmpleado.txtDni.getText();
            float saldo = Float.parseFloat(vistaEmpleado.txtSaldo.getText());
            String direccion = vistaEmpleado.txtDireccion.getText();
            String mail = vistaEmpleado.txtMail.getText();
            String telefono = vistaEmpleado.txtTelefono.getText();
            String descripcion = vistaEmpleado.txtDescripcion.getText();
            float sueldo = Float.parseFloat(vistaEmpleado.txtSueldo.getText());
            String categoria = vistaEmpleado.txtCategoria.getText();
            float periodo = Float.parseFloat(vistaEmpleado.txtPeriodo.getText());
            
            int id_empleado = Integer.parseInt(vistaEmpleado.txtIdEmpleado.getText());
            int id_persona = Integer.parseInt(vistaEmpleado.txtIdPersona.getText());
            
            Empleado emp = new Empleado(
                    id_persona, nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, 
                    descripcion, id_empleado, sueldo, categoria, periodo
            );
            
            ctrlEmpleadoS.actualizar(emp);
        }
        
        if(e.getSource() == vistaEmpleado.btnBorrar){
            int id_persona = Integer.parseInt(vistaEmpleado.txtIdPersona.getText());
            
            ctrlEmpleadoS.darDeBaja(id_persona);
        }
    }
    
}
