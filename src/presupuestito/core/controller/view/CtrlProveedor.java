/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import presupuestito.core.controller.personas.CtrlProveedorSingleton;
import presupuestito.core.model.personas.Proveedor;
import presupuestito.core.view.VentanaProveedor;

/**
 *
 * @author Luis
 */
public class CtrlProveedor implements ActionListener{
    VentanaProveedor vistaProveedor = new VentanaProveedor();
    private CtrlProveedorSingleton ctrlProveedorS;
    
    public CtrlProveedor(VentanaProveedor vistaProovedor){
        this.vistaProveedor = vistaProovedor;
        this.ctrlProveedorS = CtrlProveedorSingleton.getInstance();
        vistaProovedor.btnGuardar.addActionListener(this);
        vistaProovedor.btnListar.addActionListener(this);
        vistaProovedor.btnModificar.addActionListener(this);
        vistaProovedor.btnGuardaModificar.addActionListener(this);
        vistaProovedor.btnBorrar.addActionListener(this);
    }

    public void llenarTabla(JTable tablaE){
        DefaultTableModel modeloT = new DefaultTableModel();
        tablaE.setModel(modeloT);
        
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("APODO");
        modeloT.addColumn("APELLIDO");
        modeloT.addColumn("DNI");
        modeloT.addColumn("SALDO");
        modeloT.addColumn("DIRECCION");
        modeloT.addColumn("MAIL");
        modeloT.addColumn("TELEFONO");
        modeloT.addColumn("DESCRIPCION");
        modeloT.addColumn("TEL_CONTACTO");
        modeloT.addColumn("NOM_CONTACTO");
        
        modeloT.addColumn("id_proveedor");
        modeloT.addColumn("id_persona");
        
        Object[] columna = new Object[13];
        
        //almacena el numero de registro que se recupera de la base de datos
//        int numRegistro = ctrlProveedorS.listarProveedorActivos().size();
//        
//        for (int i = 0; i < numRegistro; i++){
//            columna[0] = ctrlProveedorS.listarProveedorActivos().get(i).getNombre();
//            columna[1] = ctrlProveedorS.listarProveedorActivos().get(i).getApodo();
//            columna[2] = ctrlProveedorS.listarProveedorActivos().get(i).getApellido();
//            columna[3] = ctrlProveedorS.listarProveedorActivos().get(i).getDni_cuit();
//            columna[4] = ctrlProveedorS.listarProveedorActivos().get(i).getSaldo();
//            columna[5] = ctrlProveedorS.listarProveedorActivos().get(i).getDireccion();
//            columna[6] = ctrlProveedorS.listarProveedorActivos().get(i).getMail();
//            columna[7] = ctrlProveedorS.listarProveedorActivos().get(i).getTelefono();
//            columna[8] = ctrlProveedorS.listarProveedorActivos().get(i).getDescripcion();
//            columna[9] = ctrlProveedorS.listarProveedorActivos().get(i).getTel_contacto();
//            columna[10] = ctrlProveedorS.listarProveedorActivos().get(i).getNombre_contacto();
//            columna[11] = ctrlProveedorS.listarProveedorActivos().get(i).getId_proveedor();
//            vistaProveedor.jtProveedores.getColumnModel().getColumn(11).setMinWidth(0);
//            vistaProveedor.jtProveedores.getColumnModel().getColumn(11).setMaxWidth(0);
//            columna[12] = ctrlProveedorS.listarProveedorActivos().get(i).getId_persona();
//            vistaProveedor.jtProveedores.getColumnModel().getColumn(12).setMinWidth(0);
//            vistaProveedor.jtProveedores.getColumnModel().getColumn(12).setMaxWidth(0);
//            
//            modeloT.addRow(columna);
//        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vistaProveedor.btnGuardar){
            String nombre = vistaProveedor.txtNombre.getText();
            String apodo = vistaProveedor.txtApodo.getText();
            String apellido = vistaProveedor.txtApellido.getText();
            String dni_cuit = vistaProveedor.txtDni.getText();
            float saldo = Float.parseFloat(vistaProveedor.txtSaldo.getText());
            String direccion = vistaProveedor.txtDireccion.getText();
            String mail = vistaProveedor.txtMail.getText();
            String telefono = vistaProveedor.txtTelefono.getText();
            String descripcion = vistaProveedor.txtDescripcion.getText();
            String tel_cont = vistaProveedor.txtTelContacto.getText();
            String nom_cont = vistaProveedor.txtNomContacto.getText();
            
            Proveedor prov = new Proveedor(
                    nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, 
                    descripcion, tel_cont, nom_cont
            );
            
            ctrlProveedorS.crear(prov);
        }
        
        if(e.getSource() == vistaProveedor.btnListar){
            llenarTabla(vistaProveedor.jtProveedores);
        }
        
        if(e.getSource() == vistaProveedor.btnModificar){
            vistaProveedor.txtNombre.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 0).toString());
            vistaProveedor.txtApodo.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 1).toString());
            vistaProveedor.txtApellido.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 2).toString());
            vistaProveedor.txtDni.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 3).toString());
            vistaProveedor.txtSaldo.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 4).toString());
            vistaProveedor.txtDireccion.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 5).toString());
            vistaProveedor.txtMail.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 6).toString());
            vistaProveedor.txtTelefono.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 7).toString());
            vistaProveedor.txtDescripcion.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 8).toString());
            vistaProveedor.txtTelContacto.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 9).toString());
            vistaProveedor.txtNomContacto.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 10).toString());
            vistaProveedor.txtIdProveedor.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 11).toString());
            vistaProveedor.txtIdPersona.setText(vistaProveedor.jtProveedores.getValueAt(vistaProveedor.jtProveedores.getSelectedRow(), 12).toString());
        }
        
        if(e.getSource() == vistaProveedor.btnGuardaModificar){
            String nombre = vistaProveedor.txtNombre.getText();
            String apodo = vistaProveedor.txtApodo.getText();
            String apellido = vistaProveedor.txtApellido.getText();
            String dni_cuit = vistaProveedor.txtDni.getText();
            float saldo = Float.parseFloat(vistaProveedor.txtSaldo.getText());
            String direccion = vistaProveedor.txtDireccion.getText();
            String mail = vistaProveedor.txtMail.getText();
            String telefono = vistaProveedor.txtTelefono.getText();
            String descripcion = vistaProveedor.txtDescripcion.getText();
            String telContacto = vistaProveedor.txtTelContacto.getText();
            String nomContacto = vistaProveedor.txtNomContacto.getText();
            int id_proveedor = Integer.parseInt(vistaProveedor.txtIdProveedor.getText());
            int id_persona = Integer.parseInt(vistaProveedor.txtIdPersona.getText());
            
            Proveedor prov = new Proveedor(
                    id_persona, nombre, apodo, apellido, dni_cuit, saldo,
                    direccion, mail, telefono, 
                    descripcion, id_proveedor, telContacto, nomContacto
            );
            
            ctrlProveedorS.actualizar(prov);
        }
        
        if(e.getSource() == vistaProveedor.btnBorrar){
            int id_persona = Integer.parseInt(vistaProveedor.txtIdPersona.getText());
            
//            ctrlProveedorS.borrarProveedor(id_persona);
        }
    }
}
