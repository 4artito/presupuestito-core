/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presupuestito.core.controller.interfaces;

import java.util.List;
import presupuestito.core.model.personas.Persona;

/**
 *
 * @author Luis
 */
public interface ABLM {
    
    public void crear(Persona p);
    
    public List<? extends Persona> listar(int estado, int limit, int offset); 
    
    public void actualizar(Persona p);
    
    public void darDeBaja(int id);
    
    public void darDeAlta(int id);
}
